/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "GenericTwinArcGISConfigParser.h"
#include "GenericTwinArcGISConfig.h"

namespace GenericTwin
{
FArcGISConfigParser::FArcGISConfigParser(FArcGISConfig &Config)
	:	m_Config(Config)
{
}

void FArcGISConfigParser::Parse(const TSharedPtr<FJsonObject> &Parameters)
{
	TMap<FString, uint32> mapTypeMap =	{	{TEXT("global"), static_cast<uint32> (EArcGISMapType::Global)}
										,	{TEXT("local"), static_cast<uint32> (EArcGISMapType::Local)}
										};

	TMap<FString, uint32> baseMapTypeMap =	{	{TEXT("image"), static_cast<uint32> (EBaseMapTypes::ImageLayer)}
											,	{TEXT("basemap"), static_cast<uint32> (EBaseMapTypes::Basemap)}
											,	{TEXT("vector_tile"), static_cast<uint32> (EBaseMapTypes::VectorTileLayer)}
											};

	TMap<FString, uint32> baseMapStyleMap =	{	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISImagery)}
											,	{TEXT("imagery_standard"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISImageryStandard)}
											,	{TEXT("imagery_labels"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISImageryLabels)}
											,	{TEXT("light_grey"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISLightGray)}
											,	{TEXT("light_grey_base"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISLightGrayBase)}
											,	{TEXT("light_grey_label"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISLightGrayLabels)}
											,	{TEXT("dark_grey"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISDarkGray)}
											,	{TEXT("dark_grey_base"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISDarkGrayBase)}
											,	{TEXT("dark_grey_labels"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISDarkGrayLabels)}
											,	{TEXT("navigation"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISNavigation)}
											,	{TEXT("navigation_night"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISNavigationNight)}
											,	{TEXT("streets"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISStreets)}
											,	{TEXT("streets_night"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISStreetsNight)}
											,	{TEXT("streets_relief"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISStreetsRelief)}
											,	{TEXT("topographic"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISTopographic)}
											,	{TEXT("oceans"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISOceans)}
											,	{TEXT("oceans_base"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISOceansBase)}
											,	{TEXT("oceans_labels"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISOceansLabels)}
											,	{TEXT("terrain"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISTerrain)}
											,	{TEXT("terrain_base"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISTerrainBase)}
											,	{TEXT("terrain_detail"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISTerrainDetail)}
											,	{TEXT("community"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISCommunity)}
											,	{TEXT("charted_territory"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISChartedTerritory)}
											,	{TEXT("colored_pencil"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISColoredPencil)}
											,	{TEXT("Nova"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISNova)}
											,	{TEXT("modern_antique"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISModernAntique)}
											,	{TEXT("midcentury"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISMidcentury)}
											,	{TEXT("newspaper"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISNewspaper)}
											,	{TEXT("hillshade_light"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISHillshadeLight)}
											,	{TEXT("hillshade_dark"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISHillshadeDark)}
											,	{TEXT("street_relief_base"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISStreetsReliefBase)}
											,	{TEXT("topographic_base"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISTopographicBase)}
											,	{TEXT("charted_territory_base"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISChartedTerritoryBase)}
											,	{TEXT("modern_antique_base"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISModernAntiqueBase)}
											,	{TEXT("human_geography_base"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISHumanGeographyBase)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISHumanGeographyDetail)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISHumanGeographyLabels)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISHumanGeographyDark)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISHumanGeographyDarkBase)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISHumanGeographyDarkDetail)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::ArcGISHumanGeographyDarkLabels)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMStandard)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMStandardRelief)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMStandardReliefBase)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMStreets)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMStreetsRelief)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMLightGray)},
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMLightGrayBase)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMLightGrayLabels)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMDarkGray)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMDarkGrayBase)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMDarkGrayLabels)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMStreetsReliefBase)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMBlueprint)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMHybrid)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMHybridDetail)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMNavigation)}
											// ,	{TEXT("imagery"), static_cast<uint32> (EArcGISBasemapStyle::OSMNavigationDark)}
											};

	// Parse global values
	{

		TArray<FJsonObjectMember> members = {	{EJsonDataType::String, TEXT("api_key"), &m_Config.APIKey}
											,	{EJsonDataType::Enumerator, TEXT("map_type"), &m_Config.MapType, true, mapTypeMap}
											,	{EJsonDataType::Enumerator, TEXT("base_map_style"), &m_Config.BaseMapStyle, true, baseMapStyleMap}
											,	{EJsonDataType::Integer, TEXT("wkid"), &m_Config.WKID}
											,	{EJsonDataType::String, TEXT("elevation_source_name"), &m_Config.ElevationSourceName, true}
											,	{EJsonDataType::String, TEXT("elevation_source_url"), &m_Config.ElevationSourceUrl, true}
		};

		GenericTwin::FJsonParser::Parse(Parameters, members);
	}

	// Parse layer configurations
	TMap<FString, uint32> layerTypeMap =	{	{TEXT("image"), static_cast<uint32> (EArcGISLayerType::ArcGISImageLayer)}
											,	{TEXT("3d_object_scene"), static_cast<uint32> (EArcGISLayerType::ArcGIS3DObjectSceneLayer)}
											,	{TEXT("integrated_mesh"), static_cast<uint32> (EArcGISLayerType::ArcGISIntegratedMeshLayer)}
											,	{TEXT("vector_tile"), static_cast<uint32> (EArcGISLayerType::ArcGISVectorTileLayer)}
											,	{TEXT("building"), static_cast<uint32> (EArcGISLayerType::ArcGISBuildingSceneLayer)}
											};

	const int32 numLayers = PushArray(TEXT("layers"));
	if(numLayers > 0)
	{
		for(int32 i = 0; i < numLayers; ++i)
		{
			FArcGISLayerConfig layerCfg;
			if	(	ParseArrayValue	(	i
									,	{	{EJsonDataType::String, TEXT("name"), &layerCfg.Name}
										,	{EJsonDataType::String, TEXT("url"), &layerCfg.Url}
										,	{EJsonDataType::Enumerator, TEXT("type"), &layerCfg.Type, true, layerTypeMap}
										,	{EJsonDataType::String, TEXT("opacity"), &layerCfg.Opacity, true}
										,	{EJsonDataType::Bool, TEXT("visible"), &layerCfg.Visible, true}
										}
									)
				)
			{
				m_Config.Layers.Add(layerCfg);
			}
		}

		PopArray();
	}

}

#if 0

        ArcGISImagery = 0,
        ArcGISImageryStandard = 1,
        ArcGISImageryLabels = 2,
        ArcGISLightGray = 3,
        ArcGISLightGrayBase = 4,
        ArcGISLightGrayLabels = 5,
        ArcGISDarkGray = 6,
        ArcGISDarkGrayBase = 7,
        ArcGISDarkGrayLabels = 8,
        ArcGISNavigation = 9,
        ArcGISNavigationNight = 10,
        ArcGISStreets = 11,
        ArcGISStreetsNight = 12,
        ArcGISStreetsRelief = 13,
        ArcGISTopographic = 14,
        ArcGISOceans = 15,
        ArcGISOceansBase = 16,
        ArcGISOceansLabels = 17,
        ArcGISTerrain = 18,
        ArcGISTerrainBase = 19,
        ArcGISTerrainDetail = 20,
        ArcGISCommunity = 21,
        ArcGISChartedTerritory = 22,
        ArcGISColoredPencil = 23,
        ArcGISNova = 24,
        ArcGISModernAntique = 25,
        ArcGISMidcentury = 26,
        ArcGISNewspaper = 27,
        ArcGISHillshadeLight = 28,
        ArcGISHillshadeDark = 29,
        ArcGISStreetsReliefBase = 30,
        ArcGISTopographicBase = 31,
        ArcGISChartedTerritoryBase = 32,
        ArcGISModernAntiqueBase = 33,
        ArcGISHumanGeographyBase = 35,
        ArcGISHumanGeographyDetail = 36,
        ArcGISHumanGeographyLabels = 37,
        ArcGISHumanGeographyDark = 38,
        ArcGISHumanGeographyDarkBase = 39,
        ArcGISHumanGeographyDarkDetail = 40,
        ArcGISHumanGeographyDarkLabels = 41,
        OSMStandard = 101,
        OSMStandardRelief = 102,
        OSMStandardReliefBase = 103,
        OSMStreets = 104,
        OSMStreetsRelief = 105,
        OSMLightGray = 106,
        OSMLightGrayBase = 107,
        OSMLightGrayLabels = 108,
        OSMDarkGray = 109,
        OSMDarkGrayBase = 110,
        OSMDarkGrayLabels = 111,
        OSMStreetsReliefBase = 112,
        OSMBlueprint = 113,
        OSMHybrid = 114,
        OSMHybridDetail = 115,
        OSMNavigation = 116,
        OSMNavigationDark = 117
 

#endif


}	//	namespace GenericTwin
