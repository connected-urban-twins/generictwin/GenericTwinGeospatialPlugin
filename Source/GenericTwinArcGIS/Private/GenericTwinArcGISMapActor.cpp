/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/


#include "GenericTwinArcGISMapActor.h"
#include "GenericTwinArcGISCameraPawn.h"

#include "ArcGISMapsSDK/BlueprintNodes/GameEngine/Map/ArcGISBasemap.h"
#include "ArcGISMapsSDK/BlueprintNodes/GameEngine/Map/ArcGISMap.h"
#include "ArcGISMapsSDK/BlueprintNodes/GameEngine/Map/ArcGISMapType.h"
#include "ArcGISMapsSDK/BlueprintNodes/GameEngine/Geometry/ArcGISSpatialReference.h"
#include "ArcGISMapsSDK/BlueprintNodes/GameEngine/Elevation/ArcGISImageElevationSource.h"
#include "ArcGISMapsSDK/BlueprintNodes/GameEngine/Layers/ArcGISImageLayer.h"
#include "ArcGISMapsSDK/BlueprintNodes/GameEngine/Map/ArcGISMapElevation.h"
#include "ArcGISMapsSDK/BlueprintNodes/GameEngine/Extent/ArcGISExtentCircle.h"
#include "ArcGISMapsSDK/BlueprintNodes/GameEngine/Layers/ArcGIS3DObjectSceneLayer.h"

#include "ArcGISMapsSDK/BlueprintNodes/GameEngine/Layers/Base\ArcGISLayerCollection.h"

DEFINE_LOG_CATEGORY(LogGenericTwinArcGISMapActor);

void AGenericTwinArcGISMapActor::OnArcGISMapComponentChanged(UArcGISMapComponent* InMapComponent)
{
	AArcGISActor::OnArcGISMapComponentChanged(InMapComponent);

	UE_LOG(LogGenericTwinArcGISMapActor, Log, TEXT("OnArcGISMapComponentChanged"));

	if (MapComponent)
	{
//		SetupArcGISMap();
	}
}


void AGenericTwinArcGISMapActor::SetupArcGISMap(const GenericTwin::FArcGISConfig &Config)
{
	if (MapComponent == 0)
    {
    	UE_LOG(LogGenericTwinArcGISMapActor, Log, TEXT("No MapComponent available"));
        return;
    }

	const EArcGISMapType::Type mapType = Config.MapType;
	UArcGISMap *arcGISMap = UArcGISMap::CreateArcGISMapWithMapType(Config.MapType);

	// Add a basemap
	const EArcGISBasemapStyle::Type basemapType = Config.BaseMapStyle;
	UArcGISBasemap *arcGISBasemap = UArcGISBasemap::CreateArcGISBasemapWithBasemapStyle(basemapType, Config.APIKey);

	// Set the basemap
	arcGISMap->SetBasemap(arcGISBasemap);

	if(Config.ElevationSourceUrl.IsEmpty() == false)
	{
		auto elevationSource = UArcGISImageElevationSource::CreateArcGISImageElevationSourceWithName(Config.ElevationSourceUrl, Config.ElevationSourceName, "");
		auto mapElevation = UArcGISMapElevation::CreateArcGISMapElevationWithElevationSource(elevationSource);
		arcGISMap->SetElevation(mapElevation);
	}

	for(const GenericTwin::FArcGISLayerConfig &layerCfg : Config.Layers)
	{
		switch(layerCfg.Type)
		{
			case EArcGISLayerType::ArcGIS3DObjectSceneLayer:
				{
					auto buildingLayer = UArcGIS3DObjectSceneLayer::CreateArcGIS3DObjectSceneLayerWithProperties(layerCfg.Url, layerCfg.Name, layerCfg.Opacity, layerCfg.Visible, "");
					arcGISMap->GetLayers()->Add(buildingLayer);
				}
				break;
		}
	}

	// Create extent
	if (mapType == EArcGISMapType::Local)
	{
		// Set this to true to enable an extent on the map component
		MapComponent->SetIsExtentEnabled(true);

		auto sr = UArcGISSpatialReference::WGS84();
		auto extentCenter = UArcGISPoint::CreateArcGISPointWithXYZSpatialReference(m_InitialLongitude, m_InitialLatitude, 3000, sr);
		auto extent = UArcGISExtentCircle::CreateArcGISExtentCircle(extentCenter, 10000);
		arcGISMap->SetClippingArea(extent);
	}

	AGenericTwinArcGISCameraPawn *pawn = Cast<AGenericTwinArcGISCameraPawn>(UGameplayStatics::GetActorOfClass(GetWorld(), AGenericTwinArcGISCameraPawn::StaticClass()));
	if (pawn)
	{
		auto PawnLocationComponent = Cast<UArcGISLocationComponent>(pawn->GetComponentByClass(UArcGISLocationComponent::StaticClass()));

		auto Position = UArcGISPoint::CreateArcGISPointWithXYZSpatialReference(m_InitialLongitude, m_InitialLatitude, 3000, UArcGISSpatialReference::WGS84());
		PawnLocationComponent->SetPosition(Position);
	}

	FArcGISViewOptions viewOptions{true};

	// Set the map and view options
	MapComponent->GetView()->SetViewOptions(viewOptions);
	MapComponent->SetMap(arcGISMap);

	UE_LOG(LogGenericTwinArcGISMapActor, Log, TEXT("SetupArcGISMap done"));

}
