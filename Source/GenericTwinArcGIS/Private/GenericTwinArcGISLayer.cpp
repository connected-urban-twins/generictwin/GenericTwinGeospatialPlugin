/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "GenericTwinArcGISLayer.h"
#include "GenericTwinArcGISConfigParser.h"

#include "GenericTwinArcGISMapActor.h"
#include "GenericTwinArcGISCameraPawn.h"

#include "TwinWorldContext.h"

#include "GeoReferencingSystem.h"
#include "Kismet/GameplayStatics.h"

#include "ArcGISMapsSDK/Actors/ArcGISMapActor.h"
#include "ArcGISMapsSDK/BlueprintNodes/GameEngine/Geometry/ArcGISSpatialReference.h"

DEFINE_LOG_CATEGORY(LogGenericTwinArcGISLayer);

const SLayerMetaData& UGenericTwinArcGISLayer::GetLayerMetaData()
{
	static SLayerMetaData layerMetaData =	{	TEXT("ArcGIS")
											,	FText::FromString(TEXT("ArcGIS Layer"))
											,	FText::FromString(TEXT("Layer for providing ArcGIS content"))
											,	ELayerContributionType::Visualisation
											,	ELayerCreationType::RuntimeOnce
											};

	return layerMetaData;
}

ULayerBase* UGenericTwinArcGISLayer::Create(FTwinWorldContext &TwinWorldCtx, const TSharedPtr<FJsonObject> &Parameters)
{
	UGenericTwinArcGISLayer *layer = NewObject<UGenericTwinArcGISLayer>(TwinWorldCtx.World, FName(), RF_Standalone);

	if(layer)
	{
		layer->OnConstruction(GetLayerMetaData(), ELayerPriority::Medium);
		GenericTwin::FArcGISConfigParser cfgParser(layer->m_Config);
		cfgParser.Parse(Parameters);
	}

	return layer;
}


UGenericTwinArcGISLayer::UGenericTwinArcGISLayer()
{
}

UGenericTwinArcGISLayer::~UGenericTwinArcGISLayer()
{
}

void UGenericTwinArcGISLayer::InitializeLayer(FTwinWorldContext &TwinWorldCtx)
{
	m_InitState = EInitState::SpawnCamera;
	// m_BaseMapActor = TwinWorldCtx.World->SpawnActor<AArcGISMapActor>(FVector::ZeroVector, FRotator::ZeroRotator, FActorSpawnParameters());
	// return;
	// if(m_BaseMapActor)
	// {
	// 	m_BaseMapActor->GetMapComponent()->SetMapType(EArcGISMapType::Local);
	// 	m_BaseMapActor->GetMapComponent()->SetAPIKey(m_APIKey);
	// 	auto sr = UArcGISSpatialReference::WGS84();
	// 	auto pos = UArcGISPoint::CreateArcGISPointWithXYZSpatialReference(TwinWorldCtx.GeoReferencingSystem->OriginLongitude, TwinWorldCtx.GeoReferencingSystem->OriginLatitude, TwinWorldCtx.GeoReferencingSystem->OriginAltitude, sr);
	// 	m_BaseMapActor->GetMapComponent()->SetOriginPosition(pos);

	// 	//m_InitState = EInitState::NotifyLevelsChanged;
	// }
	// else
	// {
	// 	m_InitState = EInitState::InitFailed;
	// }

}

void UGenericTwinArcGISLayer::UpdateView(FTwinWorldContext &TwinWorldCtx, float DeltaSeconds)
{
	if(isInitialized(TwinWorldCtx) && TwinWorldCtx.Pawn)
	{
		FTransform transform;
		transform.SetTranslation(TwinWorldCtx.Pawn->GetActorLocation());
		AController *fromCtrl = TwinWorldCtx.Pawn->GetController();
		AController *toCtrl = m_CameraActor->GetController();
		if(fromCtrl)
		{
			transform.SetRotation(fromCtrl->GetControlRotation().Quaternion());
		}
	 	m_CameraActor->SetActorTransform(transform);
	}

	// if(m_CameraActor && TwinWorldCtx.Pawn)
	// {
	// 	m_CameraActor->SetActorTransform(TwinWorldCtx.Pawn->GetActorTransform());
	// }
}

void UGenericTwinArcGISLayer::OnVisibilityChanged(FTwinWorldContext& TwinWorldCtx)
{
}

void UGenericTwinArcGISLayer::OnEndPlay(FTwinWorldContext &TwinWorldCtx)
{
	if(m_MapActor)
		m_MapActor->Destroy();
	
	if(m_CameraActor)
		m_CameraActor->Destroy();

	if(m_BaseMapActor)
		m_BaseMapActor->Destroy();
}

bool UGenericTwinArcGISLayer::isInitialized(FTwinWorldContext &TwinWorldCtx)
{
	bool res = false;

	switch(m_InitState)
	{
		case EInitState::SpawnCamera:
			m_CameraActor = TwinWorldCtx.World->SpawnActor<AGenericTwinArcGISCameraPawn>(FVector::ZeroVector, FRotator::ZeroRotator, FActorSpawnParameters());
			if(m_CameraActor)
			{
				m_InitState = EInitState::SpawnMapActor;
				UE_LOG(LogGenericTwinArcGISLayer, Log, TEXT("CameraPawn spawned"));
			}
			else
			{
				m_InitState = EInitState::InitFailed;
				UE_LOG(LogGenericTwinArcGISLayer, Log, TEXT("Failed to spawn CameraPawn"));
			}
			break;

		case EInitState::SpawnMapActor:
			m_BaseMapActor = TwinWorldCtx.World->SpawnActor<AArcGISMapActor>(FVector::ZeroVector, FRotator::ZeroRotator, FActorSpawnParameters());
			if(m_BaseMapActor)
			{
				m_BaseMapActor->GetMapComponent()->SetMapType(m_Config.MapType);
				m_BaseMapActor->GetMapComponent()->SetAPIKey(m_Config.APIKey);
				auto sr = UArcGISSpatialReference::WGS84();
				auto pos = UArcGISPoint::CreateArcGISPointWithXYZSpatialReference(TwinWorldCtx.GeoReferencingSystem->OriginLongitude, TwinWorldCtx.GeoReferencingSystem->OriginLatitude, TwinWorldCtx.GeoReferencingSystem->OriginAltitude, sr);
				m_BaseMapActor->GetMapComponent()->SetOriginPosition(pos);
				m_InitState = EInitState::NotifyLevelsChanged;
				UE_LOG(LogGenericTwinArcGISLayer, Log, TEXT("BaseMapActor spawned"));
			}
			else
			{
				m_InitState = EInitState::InitFailed;
				UE_LOG(LogGenericTwinArcGISLayer, Log, TEXT("Failed to spawn BaseMapActor"));
			}

			break;

		case EInitState::NotifyLevelsChanged:
			GetWorld()->OnLevelsChanged().Broadcast();
			m_InitState = EInitState::SpawnMap;
			UE_LOG(LogGenericTwinArcGISLayer, Log, TEXT("CameraPawn spawned"));
			break;

		case EInitState::SpawnMap:
			m_MapActor = GetWorld()->SpawnActorDeferred<AGenericTwinArcGISMapActor>( AGenericTwinArcGISMapActor::StaticClass(), FTransform());
			if(m_MapActor)
			{
				m_MapActor->Initialize(m_APIKey, TwinWorldCtx.GeoReferencingSystem->OriginLongitude, TwinWorldCtx.GeoReferencingSystem->OriginLatitude);
				m_MapActor = Cast<AGenericTwinArcGISMapActor>(UGameplayStatics::FinishSpawningActor(m_MapActor, FTransform()));
				m_MapActor->SetupArcGISMap(m_Config);
				m_InitState = EInitState::Initialized;
				UE_LOG(LogGenericTwinArcGISLayer, Log, TEXT("MapActor spawned"));
				UE_LOG(LogGenericTwinArcGISLayer, Log, TEXT("Initialized successful"));
			}
			else
			{
				m_InitState = EInitState::InitFailed;
				UE_LOG(LogGenericTwinArcGISLayer, Log, TEXT("Failed to spawn MapActor"));
			}
			break;

		case EInitState::Initialized:
			res = true;
			break;
	}

	return res;
}
