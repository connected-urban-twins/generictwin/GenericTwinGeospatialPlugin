// Copyright Epic Games, Inc. All Rights Reserved.

#include "GenericTwinArcGIS.h"

#include "GenericTwinArcGISLayer.h"

#include "Layers/LayerRepository.h"


#define LOCTEXT_NAMESPACE "FGenericTwinArcGISModule"

void FGenericTwinArcGISModule::StartupModule()
{
	LayerRepository::GetInstance().RegisterLayer(UGenericTwinArcGISLayer::GetLayerMetaData(), &UGenericTwinArcGISLayer::Create);
}

void FGenericTwinArcGISModule::ShutdownModule()
{
}


void FGenericTwinArcGISModule::PostLoadCallback()
{
	LayerRepository::GetInstance().RegisterLayer(UGenericTwinArcGISLayer::GetLayerMetaData(), &UGenericTwinArcGISLayer::Create);
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FGenericTwinArcGISModule, GenericTwinArcGIS)
