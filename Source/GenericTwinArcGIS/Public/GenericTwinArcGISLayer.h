/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "Layers/LayerBase.h"
#include "Layers/LayerTypes.h"
#include "Common/JobQueue/GenericTwinJobBase.h"

#include "GenericTwinArcGISConfig.h"

#include "GenericTwinArcGISLayer.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogGenericTwinArcGISLayer, Log, All);

class AArcGISMapActor;
class AGenericTwinArcGISCameraPawn;
class AGenericTwinArcGISMapActor;


UCLASS()
class UGenericTwinArcGISLayer	:	public ULayerBase
{
	GENERATED_BODY()

private:

	enum EInitState
	{
		Uninitialized,
		SpawnCamera,
		SpawnMapActor,
		NotifyLevelsChanged,
		SpawnMap,
		Initialized,
		InitFailed
	};

public:

	static const SLayerMetaData& GetLayerMetaData();
	static ULayerBase* Create(FTwinWorldContext &TwinWorldCtx, const TSharedPtr<FJsonObject> &Parameters);

	UGenericTwinArcGISLayer();
	virtual ~UGenericTwinArcGISLayer();

	virtual void InitializeLayer(FTwinWorldContext &TwinWorldCtx) override;

	virtual void UpdateView(FTwinWorldContext &TwinWorldCtx, float DeltaSeconds) override;

	virtual void OnVisibilityChanged(FTwinWorldContext& TwinWorldCtx) override;

	virtual void OnEndPlay(FTwinWorldContext& TwinWorldCtx) override;

private:

	bool isInitialized(FTwinWorldContext &TwinWorldCtx);

	UPROPERTY()
	AArcGISMapActor							*m_BaseMapActor = 0;

	UPROPERTY()
	AGenericTwinArcGISMapActor				*m_MapActor = 0;

	UPROPERTY()
	AGenericTwinArcGISCameraPawn			*m_CameraActor = 0;

	EInitState								m_InitState = EInitState::Uninitialized;

	GenericTwin::FArcGISConfig				m_Config;

	FString									m_APIKey;
};
