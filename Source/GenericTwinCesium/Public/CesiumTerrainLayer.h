#pragma once

#include "Layers/LayerBase.h"
#include "Layers/LayerTypes.h"

#include "CesiumTerrainLayer.generated.h"

class ACesium3DTileset;
class ACesiumGeoreference;
class ACesiumCreditSystem;
class UCesiumIonRasterOverlay;

UCLASS()
class UCesiumTerrainLayer	:	public ULayerBase
{

	GENERATED_BODY()

public:

	static const SLayerMetaData& GetLayerMetaData();
	static ULayerBase* Create(FTwinWorldContext &TwinWorldCtx, const TSharedPtr<FJsonObject> &Parameters);

	UCesiumTerrainLayer();
	virtual ~UCesiumTerrainLayer();

	virtual void InitializeLayer(FTwinWorldContext &TwinWorldCtx) override;

	virtual void UpdateView(FTwinWorldContext &TwinWorldCtx, float DeltaSeconds) override;

	virtual void OnVisibilityChanged(FTwinWorldContext& TwinWorldCtx) override;

	virtual void OnEndPlay(FTwinWorldContext& TwinWorldCtx) override;

private:

	bool createCreditSystem(FTwinWorldContext &TwinWorldCtx);
	bool createGeoReference(FTwinWorldContext &TwinWorldCtx);

	ACesiumGeoreference			*m_CesiumGeoRef = 0;

	ACesiumCreditSystem			*m_CesiumCreditSystem = 0;

	ACesium3DTileset			*m_TileSet = 0;

	UCesiumIonRasterOverlay		*m_OverlayCmp = 0;

};
