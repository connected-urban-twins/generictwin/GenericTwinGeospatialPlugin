// Copyright Epic Games, Inc. All Rights Reserved.

#include "GenericTwinCesium.h"

#include "CesiumBuildingLayer.h"
#include "CesiumTerrainLayer.h"

#include "Layers/LayerRepository.h"


#define LOCTEXT_NAMESPACE "FGenericTwinCesiumModule"

void FGenericTwinCesiumModule::StartupModule()
{
	LayerRepository::GetInstance().RegisterLayer(UCesiumBuildingLayer::GetLayerMetaData(), &UCesiumBuildingLayer::Create);
	LayerRepository::GetInstance().RegisterLayer(UCesiumTerrainLayer::GetLayerMetaData(), &UCesiumTerrainLayer::Create);
}

void FGenericTwinCesiumModule::ShutdownModule()
{
}


void FGenericTwinCesiumModule::PostLoadCallback()
{
	LayerRepository::GetInstance().RegisterLayer(UCesiumBuildingLayer::GetLayerMetaData(), &UCesiumBuildingLayer::Create);
	LayerRepository::GetInstance().RegisterLayer(UCesiumTerrainLayer::GetLayerMetaData(), &UCesiumTerrainLayer::Create);
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FGenericTwinCesiumModule, GenericTwinCesium)
