
#include "CesiumBuildingLayer.h"

#include "TwinWorldContext.h"

#include "CesiumCreditSystem.h"
#include "Cesium3DTileset.h"
#include "CesiumGeoreference.h"
#include "CesiumIonRasterOverlay.h"

#include "GeoReferencingSystem.h"
#include "Kismet/GameplayStatics.h"


const SLayerMetaData& UCesiumBuildingLayer::GetLayerMetaData()
{
	static SLayerMetaData layerMetaData =	{	TEXT("CesiumBuilding")
											,	FText::FromString(TEXT("Cesium Buildings"))
											,	FText::FromString(TEXT("Layer for providing Cesium buildings content"))
											,	ELayerContributionType::Visualisation
											,	ELayerCreationType::RuntimeOnce
											};

	return layerMetaData;
}

ULayerBase* UCesiumBuildingLayer::Create(FTwinWorldContext &TwinWorldCtx, const TSharedPtr<FJsonObject> &Parameters)
{
	UCesiumBuildingLayer *layer = NewObject<UCesiumBuildingLayer>(TwinWorldCtx.World, FName(), RF_Standalone);

	if(layer)
	{
		layer->OnConstruction(GetLayerMetaData(), ELayerPriority::Medium);
	}

	return layer;
}



UCesiumBuildingLayer::UCesiumBuildingLayer()
{
}

UCesiumBuildingLayer::~UCesiumBuildingLayer()
{
}

void UCesiumBuildingLayer::InitializeLayer(FTwinWorldContext &TwinWorldCtx)
{
	if	(	createCreditSystem(TwinWorldCtx)
		&&	createGeoReference(TwinWorldCtx)
		)
	{
		m_BuildingTileSet = TwinWorldCtx.World->SpawnActorDeferred<ACesium3DTileset>(ACesium3DTileset::StaticClass(), FTransform(), 0, 0, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		if(m_BuildingTileSet)
		{
			m_BuildingTileSet->SetGeoreference(m_CesiumGeoRef);
			m_BuildingTileSet->SetCreditSystem(m_CesiumCreditSystem);

			m_BuildingTileSet->SetIonAssetID(96188);

			m_BuildingTileSet = Cast<ACesium3DTileset> (UGameplayStatics::FinishSpawningActor(m_BuildingTileSet, FTransform()));
		}
	}
}

void UCesiumBuildingLayer::UpdateView(FTwinWorldContext &TwinWorldCtx, float DeltaSeconds)
{
}

void UCesiumBuildingLayer::OnVisibilityChanged(FTwinWorldContext& TwinWorldCtx)
{
	m_BuildingTileSet->SetActorHiddenInGame(!m_isVisible);
}

void UCesiumBuildingLayer::OnEndPlay(FTwinWorldContext &TwinWorldCtx)
{
}


bool UCesiumBuildingLayer::createCreditSystem(FTwinWorldContext &TwinWorldCtx)
{
	m_CesiumCreditSystem = ACesiumCreditSystem::GetDefaultCreditSystem(TwinWorldCtx.World);
	return m_CesiumCreditSystem != 0;
}

bool UCesiumBuildingLayer::createGeoReference(FTwinWorldContext &TwinWorldCtx)
{
	m_CesiumGeoRef = ACesiumGeoreference::GetDefaultGeoreference(TwinWorldCtx.World);

	if(m_CesiumGeoRef)
	{
		m_CesiumGeoRef->SetOriginLongitudeLatitudeHeight( FVector(TwinWorldCtx.GeoReferencingSystem->OriginLongitude, TwinWorldCtx.GeoReferencingSystem->OriginLatitude, TwinWorldCtx.GeoReferencingSystem->OriginAltitude) );
	}

	return m_CesiumGeoRef != 0;
}
