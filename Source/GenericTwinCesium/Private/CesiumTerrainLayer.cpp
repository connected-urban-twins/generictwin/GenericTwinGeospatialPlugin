
#include "CesiumTerrainLayer.h"

#include "TwinWorldContext.h"

#include "CesiumCreditSystem.h"
#include "Cesium3DTileset.h"
#include "CesiumGeoreference.h"
#include "CesiumIonRasterOverlay.h"

#include "GeoReferencingSystem.h"
#include "Kismet/GameplayStatics.h"


const SLayerMetaData& UCesiumTerrainLayer::GetLayerMetaData()
{
	static SLayerMetaData layerMetaData =	{	TEXT("CesiumTerrain")
											,	FText::FromString(TEXT("Cesium Terrain"))
											,	FText::FromString(TEXT("Layer for providing Cesium terrain content"))
											,	ELayerContributionType::Visualisation
											,	ELayerCreationType::RuntimeOnce
											};

	return layerMetaData;
}

ULayerBase* UCesiumTerrainLayer::Create(FTwinWorldContext &TwinWorldCtx, const TSharedPtr<FJsonObject> &Parameters)
{
	UCesiumTerrainLayer *layer = NewObject<UCesiumTerrainLayer>(TwinWorldCtx.World, FName(), RF_Standalone);

	if(layer)
	{
		layer->OnConstruction(GetLayerMetaData(), ELayerPriority::Medium);
	}

	return layer;
}



UCesiumTerrainLayer::UCesiumTerrainLayer()
{
}

UCesiumTerrainLayer::~UCesiumTerrainLayer()
{
}

void UCesiumTerrainLayer::InitializeLayer(FTwinWorldContext &TwinWorldCtx)
{
	if	(	createCreditSystem(TwinWorldCtx)
		&&	createGeoReference(TwinWorldCtx)
		)
	{
		m_TileSet = TwinWorldCtx.World->SpawnActorDeferred<ACesium3DTileset>(ACesium3DTileset::StaticClass(), FTransform(), 0, 0, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		if(m_TileSet)
		{
			m_TileSet->SetGeoreference(m_CesiumGeoRef);
			m_TileSet->SetCreditSystem(m_CesiumCreditSystem);

			m_TileSet->SetIonAssetID(1);

			m_OverlayCmp = Cast<UCesiumIonRasterOverlay>( m_TileSet->AddComponentByClass(UCesiumIonRasterOverlay::StaticClass(), false, FTransform(), false) );
			if(m_OverlayCmp)
			{
				m_OverlayCmp->IonAssetID = 2;
			}


			m_TileSet = Cast<ACesium3DTileset> (UGameplayStatics::FinishSpawningActor(m_TileSet, FTransform()));
		}
	}
}

void UCesiumTerrainLayer::UpdateView(FTwinWorldContext &TwinWorldCtx, float DeltaSeconds)
{
}

void UCesiumTerrainLayer::OnVisibilityChanged(FTwinWorldContext& TwinWorldCtx)
{
	m_TileSet->SetActorHiddenInGame(!m_isVisible);
}

void UCesiumTerrainLayer::OnEndPlay(FTwinWorldContext &TwinWorldCtx)
{
}


bool UCesiumTerrainLayer::createCreditSystem(FTwinWorldContext &TwinWorldCtx)
{
	m_CesiumCreditSystem = ACesiumCreditSystem::GetDefaultCreditSystem(TwinWorldCtx.World);
	return m_CesiumCreditSystem != 0;
}

bool UCesiumTerrainLayer::createGeoReference(FTwinWorldContext &TwinWorldCtx)
{
	m_CesiumGeoRef = ACesiumGeoreference::GetDefaultGeoreference(TwinWorldCtx.World);

	if(m_CesiumGeoRef)
	{
		m_CesiumGeoRef->SetOriginLongitudeLatitudeHeight( FVector(TwinWorldCtx.GeoReferencingSystem->OriginLongitude, TwinWorldCtx.GeoReferencingSystem->OriginLatitude, TwinWorldCtx.GeoReferencingSystem->OriginAltitude) );
	}

	return m_CesiumGeoRef != 0;
}
