/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "CoreMinimal.h"

#include <functional>

class AGenericTwinOGCFeaturesVisualiser;
class UWorld;

namespace GenericTwin {

class OGCFeaturesVisualiserFactory
{
public:

	typedef std::function< TObjectPtr<AGenericTwinOGCFeaturesVisualiser> (UWorld*, const FJsonObject&) > CreateVisualiserFuncPtr;

	static OGCFeaturesVisualiserFactory& GetInstance();

	void RegisterVisualiser(const FString &Type, CreateVisualiserFuncPtr CreateFuncPtr);

	TObjectPtr<AGenericTwinOGCFeaturesVisualiser> CreateVisualiser(UWorld *World, const FString &Type, const FJsonObject &Parameters) const;

private:

	OGCFeaturesVisualiserFactory();
	~OGCFeaturesVisualiserFactory();

	static OGCFeaturesVisualiserFactory					*s_Instance;

	TMap<FString, CreateVisualiserFuncPtr>				m_RegisteredVisualisers;

};

}	//	namespace GenericTwin
