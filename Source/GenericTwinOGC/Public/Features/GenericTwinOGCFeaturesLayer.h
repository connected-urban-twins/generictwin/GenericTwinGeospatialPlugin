/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "Streaming/TileStreamingLayer.h"


#include "Common/JobQueue/TJobQueue.h"

#include "GenericTwinOGCFeaturesLayer.generated.h" 

DECLARE_LOG_CATEGORY_EXTERN(LogGenericTwinOGCFeaturesLayer, Log, All);

namespace GenericTwin {

class ILocationMapper;

}	//	namespace GenericTwin 

class AGeoReferencingSystem;
class AGenericTwinOGCFeaturesVisualiser;

UCLASS()
class UGenericTwinOGCFeaturesLayer	:	public UTileStreamingLayer
{
	GENERATED_BODY()

private:

public:

	static const SLayerMetaData& GetLayerMetaData();
	static TSharedPtr<FJsonObject> GetDefaultConfiguration();
	static ULayerBase* Create(FTwinWorldContext &TwinWorldCtx, const TSharedPtr<FJsonObject> &Parameters);

	virtual ~UGenericTwinOGCFeaturesLayer();

	/*
	*	ULayerBase
	*/
	virtual void InitializeLayer(FTwinWorldContext &TwinWorldCtx) override;

	virtual void UpdateView(FTwinWorldContext &TwinWorldCtx, float DeltaSeconds) override;

	/*
	*	UTileLayerBase
	*/
	virtual TileBasePtr CreateTile(FTwinWorldContext &TwinWorldCtx, const GenericTwin::TTileId &TileId) override;

	const FString& GetDomain() const;
	const FString& GetBaseUrl() const;
	const FString& GetRequestCRS() const;

	AGenericTwinOGCFeaturesVisualiser* CreateVisualiser();

	AGeoReferencingSystem& GetGeoReferencingSystem() const;

	float GetTileSizeM() const;

private:

	UGenericTwinOGCFeaturesLayer();

	void ExtractParameters(const FJsonObject &Parameters, FTwinWorldContext &TwinWorldCtx);
	bool ExtractVisualiser(const TSharedPtr<FJsonObject> &VisualiserObject);

	GenericTwin::ILocationMapper				*m_LocationMapper = 0;		//	using CRS specified in config

	AGeoReferencingSystem						*m_GeoReferencingSystem = 0;
	FVector										m_ReferenceLocation;

	FVector2D									m_TileSize;

	float										m_TileSizeM = 0.0f;

	FString										m_Domain;
	FString										m_BaseUrl;

	FString										m_RequestCRS;

	float										m_TileOffset = 0.0f;

	FString										m_VisualiserType;
	FJsonObject									m_VisualiserObject;
};


inline const FString& UGenericTwinOGCFeaturesLayer::GetDomain() const
{
	return m_Domain;
}

inline const FString& UGenericTwinOGCFeaturesLayer::GetBaseUrl() const
{
	return m_BaseUrl;
}

inline const FString& UGenericTwinOGCFeaturesLayer::GetRequestCRS() const
{
	return m_RequestCRS;
}

inline AGeoReferencingSystem& UGenericTwinOGCFeaturesLayer::GetGeoReferencingSystem() const
{
	return *m_GeoReferencingSystem;
}

inline float UGenericTwinOGCFeaturesLayer::GetTileSizeM() const
{
	return m_TileSizeM;
}
