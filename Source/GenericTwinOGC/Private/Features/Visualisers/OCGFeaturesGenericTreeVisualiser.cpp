/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PgattungICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "Features/Visualisers/OCGFeaturesGenericTreeVisualiser.h"

#include "Common/JobQueue/GenericTwinJobBase.h"
#include "Common/JobQueue/GenericTwinJobDispatcher.h"

#include "Components/GenericTwinCustomMeshComponent.h"
#include "Components/GenericTwinStaticMeshComponent.h"

#include "Math/UnrealMathUtility.h"
#include "Utils/FilePathUtils.h"

#include "GeoReferencingSystem.h"

DEFINE_LOG_CATEGORY(LogOCGFeaturesGenericTreeVisualiser);

AOCGFeaturesGenericTreeVisualiser::STreesConfiguration AOCGFeaturesGenericTreeVisualiser::s_TreesConfiguration;
UMaterial* AOCGFeaturesGenericTreeVisualiser::s_TrunkMaterial = 0;
UMaterial* AOCGFeaturesGenericTreeVisualiser::s_CrownMaterial = 0;

TObjectPtr<AOCGFeaturesGenericTreeVisualiser> AOCGFeaturesGenericTreeVisualiser::Create(UWorld *World, const FJsonObject &Parameters)
{
	AOCGFeaturesGenericTreeVisualiser *visualiser = 0;
	if	(	Parameters.HasTypedField<EJson::String>(TEXT("tree_config_csv"))
		&&	Parameters.HasTypedField<EJson::String>(TEXT("trunk_material"))
		&&	Parameters.HasTypedField<EJson::String>(TEXT("crown_material"))
		)
	{

		visualiser = World->SpawnActor<AOCGFeaturesGenericTreeVisualiser>(FVector::ZeroVector, FRotator::ZeroRotator, FActorSpawnParameters());

		if(visualiser)
		{
			if(visualiser->MeshComponent)
				visualiser->MeshComponent->Initialize(GenericTwin::IStaticMeshComponent::EUploadMode::Background);

			if(s_TreesConfiguration.PrimaryIdentifierKey.IsEmpty())
			{
				LoadConfiguration( GenericTwin::FPathUtils::MakeAbsolutPath(Parameters.GetStringField(TEXT("tree_config_csv")), FPaths::LaunchDir()));
			}
		}
	}

	return visualiser;
}


AOCGFeaturesGenericTreeVisualiser::AOCGFeaturesGenericTreeVisualiser()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = GenericTwin::IStaticMeshComponent::Create(this);

	if(MeshComponent)
	{
		USceneComponent *sceneCmp = MeshComponent->GetSceneComponent();
		if(sceneCmp)
		{
			SetRootComponent(sceneCmp);
			sceneCmp->SetMobility(EComponentMobility::Movable);
		}

		// MeshComponent->Construct(this, GenericTwin::IStaticMeshComponent::EUploadMode::Background);

		// MeshComponent->Initialize(GenericTwin::IStaticMeshComponent::EUploadMode::Background);
	}

	// MeshComponent = CreateDefaultSubobject<UGenericTwinCustomMeshComponent>(TEXT("MeshComponent"));
	// MeshComponent->SetMobility(EComponentMobility::Movable);

	// SetRootComponent(MeshComponent);
}

void AOCGFeaturesGenericTreeVisualiser::SetFeaturesCollection(const GenericTwin::OGCFeaturesCollection &FeaturesCollection, AGeoReferencingSystem &GeoReferencingSystem, const FVector &RefWorldLocation)
{
	class BuildTreeMeshesJob	:	public GenericTwin::JobBase
	{
	public:

		BuildTreeMeshesJob(AOCGFeaturesGenericTreeVisualiser &TreeVisualiser, const GenericTwin::OGCFeaturesCollection &FeaturesCollection, AGeoReferencingSystem &GeoReferencingSystem, const FVector &RefWorldLocation)
			:	JobBase(GenericTwin::EJobType::Background)
			,	m_TreeVisualiser(TreeVisualiser)
			,	m_FeaturesCollection(FeaturesCollection)
			,	m_GeoReferencingSystem(GeoReferencingSystem)
			,	m_RefWorldLocation(RefWorldLocation)
		{
		}

		virtual GenericTwin::TJobBasePtr Execute() override
		{
			m_TreeVisualiser.BuildTreeGeometry(m_FeaturesCollection, m_GeoReferencingSystem, m_RefWorldLocation);
			return GenericTwin::TJobBasePtr();
		}

		virtual void OnJobFinished()
		{
			m_TreeVisualiser.UploadGeometry();
		}

	private:

		AOCGFeaturesGenericTreeVisualiser		&m_TreeVisualiser;
		GenericTwin::OGCFeaturesCollection		m_FeaturesCollection;
		AGeoReferencingSystem					&m_GeoReferencingSystem;
		FVector									m_RefWorldLocation;
	};

	FGenericTwinJobDispatcher::DispatchJob(GenericTwin::TJobBasePtr( new BuildTreeMeshesJob(*this, FeaturesCollection, GeoReferencingSystem, RefWorldLocation) ));

}

void AOCGFeaturesGenericTreeVisualiser::BuildTreeGeometry(const GenericTwin::OGCFeaturesCollection &FeaturesCollection, AGeoReferencingSystem &GeoReferencingSystem, const FVector &RefWorldLocation)
{
	const float MinTrunkCircumFerence = 20.0;
	const float MinCrownDiameter = 50.0;

	UE_LOG(LogOCGFeaturesGenericTreeVisualiser, Log, TEXT("Adding trees at %s numFeatures %d"), *(RefWorldLocation.ToString()), FeaturesCollection.GetFeatures().Num());

	int32 numBroadleaf = 0;
	int32 numConifer = 0;

	for(const GenericTwin::OGCFeaturesItem &feature : FeaturesCollection.GetFeatures())
	{
		if	(	feature.GeometryType == GenericTwin::OGCFeaturesItem::Points
			&&	feature.Coordinates.Num() > 0
			&&	feature.Properties.Contains(TEXT("stammumfang"))
			&&	feature.Properties.Contains(TEXT("kronendurchmesser"))
			)
		{
			const float trunkCircumference = FCString::Atof(*(feature.Properties[TEXT("stammumfang")]));
			const float crownDiameter = FCString::Atof(*(feature.Properties[TEXT("kronendurchmesser")])) * 100.0;

			if	(	trunkCircumference > MinTrunkCircumFerence
				&&	crownDiameter > MinCrownDiameter
				)
			{
				const FString primaryId = feature.Properties.Contains(s_TreesConfiguration.PrimaryIdentifierKey) ? feature.Properties[s_TreesConfiguration.PrimaryIdentifierKey].ToLower() : FString();
				if(s_TreesConfiguration.Trees.Contains(primaryId))
				{
					FVector projectedCoord(feature.Coordinates[0], 0.0);
					FVector treeLocation;
					GeoReferencingSystem.ProjectedToEngine(projectedCoord, treeLocation);
					
					// do line trace
					double height = 0.0;
					GetGroundHeight(treeLocation, height);

					treeLocation -= RefWorldLocation;

					treeLocation.Z = height;

					const ETreeType type = s_TreesConfiguration.Trees[primaryId].TreeType;

					STrunkParameters trunkParams;

					trunkParams.WideningBase = 3.0f;
					trunkParams.TaperingBase = 0.9f;
					trunkParams.Radius = trunkCircumference / 2.0 / PI;


					switch(type)
					{
						case ETreeType::Broadleaf:
							{
								float trunkHeight = trunkCircumference * 2.0f;

								SBroadleafParameters crownParams;
								crownParams.Radius = 0.5f * crownDiameter;

								CreateTrunk(m_TrunkSection, treeLocation, trunkHeight * 1.1f, trunkParams);
								CreateBroadleafCrown(m_CrownSection, treeLocation, trunkHeight, crownParams);
								++numBroadleaf;
							}
							break;

						case ETreeType::Conifer:
							{
								float trunkHeight = crownDiameter;
								CreateTrunk(m_TrunkSection, treeLocation, trunkHeight * 1.1f, trunkParams);

								SConiferParameters crownParams;

								crownParams.Radius = 0.5f * crownDiameter;
								crownParams.Height = 1.5f * crownDiameter;

								CreateConiferCrown(m_CrownSection, treeLocation, 0.5f * crownDiameter, crownParams);
								++numConifer;
							}
							break;
					}

				}

			}
		}
	}

	if(m_TrunkSection.Indices.Num() > 0)
		CalculateNormals(m_TrunkSection);

	if(m_CrownSection.Indices.Num() > 0)
		CalculateNormals(m_CrownSection);

	UE_LOG(LogOCGFeaturesGenericTreeVisualiser, Log, TEXT("Geometry created, tv %d tt %d cv %d ct %d"), m_TrunkSection.Vertices.Num(), m_TrunkSection.Indices.Num() / 3, m_CrownSection.Vertices.Num(), m_CrownSection.Indices.Num() / 3 );
}


void AOCGFeaturesGenericTreeVisualiser::UploadGeometry()
{
	UE_LOG(LogOCGFeaturesGenericTreeVisualiser, Log, TEXT("Uploading geometry") );

	if(s_TrunkMaterial == 0)
		s_TrunkMaterial = Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), GetWorld(), TEXT("/GenericTwinGeospatialPlugin/Materials/OGCFeatures/Tree/M_SimplifiedTree_Trunk")));

	if(s_CrownMaterial == 0)
		s_CrownMaterial = Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), GetWorld(), TEXT("/GenericTwinGeospatialPlugin/Materials/OGCFeatures/Tree/M_SimplifiedTree_Crown")));

	if (m_TrunkSection.Indices.Num() > 0)
	{
		MeshComponent->AddMeshSection(m_TrunkSection.Indices, m_TrunkSection.Vertices, &m_TrunkSection.Normals, &m_TrunkSection.UV0, nullptr, nullptr, s_TrunkMaterial);

		m_TrunkSection.Clear();
	}

	if(m_CrownSection.Indices.Num() > 0)
	{
		MeshComponent->AddMeshSection(m_CrownSection.Indices, m_CrownSection.Vertices, &m_CrownSection.Normals, &m_CrownSection.UV0, nullptr, nullptr, s_CrownMaterial);

		m_CrownSection.Clear();
	}

	UE_LOG(LogOCGFeaturesGenericTreeVisualiser, Log, TEXT("Geometry uploaded") );
}

void AOCGFeaturesGenericTreeVisualiser::CreateTrunk(GenericTwin::SMeshSection &MeshSection, const FVector &TreeLocation, double TrunkHeight, const STrunkParameters &Parameters)
{
	const int32 startIndex = MeshSection.Vertices.Num();

	const float refHeight = 75.0f;

	TArray<FVector> sliceCoords = CreateSlice(Parameters.NumSegments);

	float curHeight = 0.0;

	// Create base of the trunk up to reference height where trunk radius is measured
	float dH = refHeight / Parameters.NumBaseSlices;
	for(int32 i = 0; i <= Parameters.NumBaseSlices; ++i)
	{
		const float x = (refHeight - curHeight) / refHeight;
		const float curRadius = Parameters.Radius * FMath::Pow(Parameters.WideningBase, x);

		for(int32 j = 0; j < sliceCoords.Num(); ++j)
		{
			const FVector &curCoord = sliceCoords[j];
			MeshSection.Vertices.Add( FVector(curRadius * curCoord.X, curRadius * curCoord.Y, curHeight) + TreeLocation );
			MeshSection.Normals.Add( FVector::ZeroVector );
			MeshSection.UV0.Add( FVector2D(curCoord.Z, curHeight / Parameters.TexSizeT ) );
		}

		curHeight += dH;
	}

	// Create top of the trunk from reference height
	dH = (TrunkHeight - refHeight) / Parameters.NumSlices;
	curHeight = refHeight + dH;
	for(int32 i = 1; i <= Parameters.NumSlices; ++i)
	{
		const float x = (curHeight - refHeight) / Parameters.TaperingPerHeight;
		const float curRadius = Parameters.Radius * FMath::Pow(Parameters.TaperingBase, x);

		for(int32 j = 0; j < sliceCoords.Num(); ++j)
		{
			const FVector &curCoord = sliceCoords[j];
			MeshSection.Vertices.Add( FVector(curRadius * curCoord.X, curRadius * curCoord.Y, curHeight) + TreeLocation );
			MeshSection.Normals.Add( FVector::ZeroVector );
			MeshSection.UV0.Add( FVector2D(curCoord.Z, curHeight / Parameters.TexSizeT ) );
		}

		curHeight += dH;
	}

	int32 curIdx = startIndex;
	const int32 numSlices = Parameters.NumBaseSlices + Parameters.NumSlices;
	for(int32 i = 0; i < numSlices; ++i)
	{
		for(int32 j = 0; j < Parameters.NumSegments; ++j)
		{
			MeshSection.Indices.Add(curIdx + j + sliceCoords.Num());
			MeshSection.Indices.Add(curIdx + j + 1);
			MeshSection.Indices.Add(curIdx + j);

			MeshSection.Indices.Add(curIdx + j + sliceCoords.Num() + 1);
			MeshSection.Indices.Add(curIdx + j + 1);
			MeshSection.Indices.Add(curIdx + j + sliceCoords.Num());

		}
		curIdx += sliceCoords.Num();
	}

}

void AOCGFeaturesGenericTreeVisualiser::CreateBroadleafCrown(GenericTwin::SMeshSection &MeshSection, const FVector &TreeLocation, double HeightOffset, const SBroadleafParameters &Parameters)
{
	const float colorVal = FMath::RandRange(0.0, 1.0);
	const int32 startIndex = MeshSection.Vertices.Num();

	const float crownRadius = Parameters.Radius;
	const float verticalRadius = crownRadius * FMath::RandRange(Parameters.Flattening.X, Parameters.Flattening.Y);

	HeightOffset += verticalRadius;

	TArray<FVector> sliceCoords = CreateSlice(Parameters.NumSegments);

	const float cappedPolar = 1.5f;
	const float dPolar = 2.0f * cappedPolar / Parameters.NumSlices;

	const float randFac = FMath::RandRange(0.975f, 2.1536f);

	// add "top" vertex
	MeshSection.Vertices.Add( FVector(0.0, 0.0, HeightOffset + verticalRadius) + TreeLocation );
	MeshSection.Normals.Add(FVector::ZeroVector);
	MeshSection.UV0.Add( FVector2D(colorVal) );

	float curPolar = cappedPolar;
	for(int32 i = 0; i <= Parameters.NumSlices; ++i)
	{
		const float curZ = FMath::Sin(curPolar) * verticalRadius;
		const float curRadius = FMath::Cos(curPolar) * crownRadius;// *FMath::Lerp(0.975f, 1.025f, FMath::PerlinNoise1D(i * randFac));

		const int32 curStartIndex = MeshSection.Vertices.Num();
		for(int32 j = 0; j < sliceCoords.Num(); ++j)
		{
			const FVector& curCoord = sliceCoords[j];

			if(j < sliceCoords.Num() - 1)
			{
				const FVector curPos = FVector(curRadius * curCoord.X, curRadius * curCoord.Y, curZ);
				MeshSection.Vertices.Add( curPos + FVector(0.0, 0.0, HeightOffset) + TreeLocation );
				MeshSection.Normals.Add(FVector::ZeroVector);
			}
			else
			{
				const FVector p = MeshSection.Vertices[curStartIndex];
				MeshSection.Vertices.Add(p);
				const FVector n = MeshSection.Normals[curStartIndex];
				MeshSection.Normals.Add(n);
			}

			MeshSection.UV0.Add( FVector2D(0.0f, colorVal) );
		}

		curPolar -= dPolar;
	}

	// add "top" vertex
	MeshSection.Vertices.Add( FVector(0.0, 0.0, HeightOffset + -verticalRadius) + TreeLocation );
	MeshSection.Normals.Add(FVector::ZeroVector);
	MeshSection.UV0.Add( FVector2D(colorVal) );

	int32 curIdx = startIndex;

	for(int32 i = 0; i < Parameters.NumSegments; ++i)
	{
		MeshSection.Indices.Add(curIdx);
		MeshSection.Indices.Add(curIdx + i + 2);
		MeshSection.Indices.Add(curIdx + i + 1);
	}
	curIdx++;

	const int32 numSlices = Parameters.NumSlices;
	for(int32 i = 0; i < numSlices; ++i)
	{
		for(int32 j = 0; j < Parameters.NumSegments; ++j)
		{
			MeshSection.Indices.Add(curIdx + j);
			MeshSection.Indices.Add(curIdx + j + 1);
			MeshSection.Indices.Add(curIdx + j + sliceCoords.Num());

			MeshSection.Indices.Add(curIdx + j + sliceCoords.Num());
			MeshSection.Indices.Add(curIdx + j + 1);
			MeshSection.Indices.Add(curIdx + j + sliceCoords.Num() + 1);

		}
		curIdx += sliceCoords.Num();
	}

}

void AOCGFeaturesGenericTreeVisualiser::CreateConiferCrown(GenericTwin::SMeshSection &MeshSection, const FVector &TreeLocation, double HeightOffset, const SConiferParameters &Parameters)
{
	const float colorVal = FMath::RandRange(0.0, 1.0);

	TArray<FVector> sliceCoords = CreateSlice(Parameters.NumSegments);

	float coneHeight = FMath::RandRange(Parameters.ConeSize.X, Parameters.ConeSize.Y) * Parameters.Height / Parameters.NumSlices;

	float curRadius = Parameters.Radius;
	float curZ = HeightOffset;
	for(int32 s = 0; s < Parameters.NumSlices; ++s)
	{
		const int32 curStartIdx = MeshSection.Vertices.Num();

		MeshSection.Vertices.Add( FVector(0.0, 0.0, curZ + coneHeight) + TreeLocation );
		MeshSection.Normals.Add(FVector::ZeroVector);
		MeshSection.UV0.Add( FVector2D(colorVal) );

		for(int32 i = 0; i < sliceCoords.Num() - 1; ++i)
		{
			const FVector& curCoord = sliceCoords[i];

			const FVector curPos = FVector(curRadius * curCoord.X, curRadius * curCoord.Y, curZ);
			MeshSection.Vertices.Add( curPos + TreeLocation );
			MeshSection.Normals.Add(FVector::ZeroVector);
			MeshSection.UV0.Add( FVector2D(1.0f, colorVal) );
		}

		for(int32 i = 0; i < Parameters.NumSegments; ++i)
		{
			MeshSection.Indices.Add(curStartIdx);

			const int32 i1 = i < (Parameters.NumSegments - 1) ? (curStartIdx + i + 2) : (curStartIdx + 1);
			MeshSection.Indices.Add(i1);

			MeshSection.Indices.Add(curStartIdx + i + 1);
		}

		curZ += FMath::RandRange(Parameters.ConeOffset.X, Parameters.ConeOffset.Y) * coneHeight;
		curRadius *= FMath::RandRange(Parameters.RadiusReduction.X, Parameters.RadiusReduction.Y);
		coneHeight *= FMath::RandRange(Parameters.ConeFlattening.X, Parameters.ConeFlattening.Y);
	}

}

void AOCGFeaturesGenericTreeVisualiser::CalculateNormals(GenericTwin::SMeshSection &MeshSection)
{
	const int32 numTriangles = MeshSection.Indices.Num() / 3;
	for(int32 i = 0; i < numTriangles; ++i)
	{
		const int32 ind0 = MeshSection.Indices[3 * i];
		const int32 ind1 = MeshSection.Indices[3 * i + 1];
		const int32 ind2 = MeshSection.Indices[3 * i + 2];

		FVector v0 = MeshSection.Vertices[ind0] - MeshSection.Vertices[ind1];
		FVector v1 = MeshSection.Vertices[ind2] - MeshSection.Vertices[ind1];

		v0.Normalize();
		v1.Normalize();

		FVector nrm = FVector::CrossProduct(v0, v1);
		MeshSection.Normals[ind0] += nrm;
		MeshSection.Normals[ind1] += nrm;
		MeshSection.Normals[ind2] += nrm;
	}

	for(FVector &nrm : MeshSection.Normals)
		nrm.Normalize();
}


TArray<FVector> AOCGFeaturesGenericTreeVisualiser::CreateSlice(int32 NumSegments)
{
	TArray<FVector> slice;
	const double deltaAng = 2.0 * PI / NumSegments;
	double curAng = 0.0;

	const double deltaS = 1.0 / NumSegments;
	double curS = 0.0;
	for(int32 i = 0; i <= NumSegments; ++i)
	{
		const double curX = FMath::Cos(curAng);
		const double curY = FMath::Sin(curAng);

		slice.Add(FVector(curX, curY, curS));

		curAng += deltaAng;
		curS += deltaS;
	}

	return slice;
}

void AOCGFeaturesGenericTreeVisualiser::LoadConfiguration(const FString &ConfigurationFile)
{
	TArray<FString> lines;
	if(FFileHelper::LoadFileToStringArray(lines, *ConfigurationFile))
	{
		if(lines.Num() > 0)
		{
			const FString delimiter(TEXT("|"));
			TArray<FString> parts;
			lines[0].ParseIntoArray(parts, *delimiter, false);

			if(parts.Num() >= 3)
			{
				s_TreesConfiguration.PrimaryIdentifierKey = parts[0];
				s_TreesConfiguration.SecondaryIdentifierKey = parts[1];
				for(int32 i = 1; i < lines.Num(); ++i)
				{
					lines[i].ParseIntoArray(parts, *delimiter, false);

					if(parts.Num() >= 3)
					{
						FString typeStr = parts[2];
						ETreeType type = ETreeType::Undefined;

						if(typeStr.Compare("broadleaf", ESearchCase::IgnoreCase) == 0)
							type = ETreeType::Broadleaf;
						else if(typeStr.Compare("conifer", ESearchCase::IgnoreCase) == 0)
							type = ETreeType::Conifer;

						if(type != ETreeType::Undefined)
						{
							STreeConfigItem item;
							item.SecondaryIdentifier = parts[1];
							item.TreeType = type;

							s_TreesConfiguration.Trees.Add(parts[0], item);
						}
					}
				}
				UE_LOG(LogOCGFeaturesGenericTreeVisualiser, Log, TEXT("Found %d tree types"), s_TreesConfiguration.Trees.Num());
			}
		}
		else
		{
			UE_LOG(LogOCGFeaturesGenericTreeVisualiser, Warning, TEXT("No lines found"));
		}
	}
	else
	{
		UE_LOG(LogOCGFeaturesGenericTreeVisualiser, Warning, TEXT("File %s not found"), *ConfigurationFile);
	}
}

uint32 AOCGFeaturesGenericTreeVisualiser::GetEstimatedGPUMemoryUsage() const
{
	return m_EstimatedGPUUsage;
}

