/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "Features/GenericTwinOGCFeaturesVisualiser.h"

#include "OCGFeaturesTreeMeshVisualiser.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogOCGFeaturesTreeMeshVisualiser, Log, All);

UCLASS()
class GENERICTWINOGC_API AOCGFeaturesTreeMeshVisualiser	:	public AGenericTwinOGCFeaturesVisualiser
{
	GENERATED_BODY()

	struct STreeMeshConfigItem
	{
		FString					SecondaryIdentifier;

		FString					MeshReference;
		float					TrunkRadius = 0.0f;
		float					CrownRadius = 0.0f;
		float					TreeHeight = 0.0f;

	};


	struct STreeConfigItem
	{
		FString								SecondaryIdentifier;

		TArray<STreeMeshConfigItem>			Meshes;

	};

	struct STreesConfiguration
	{
		bool								IsLoaded = false;
		FString								PrimaryIdentifierKey;
		FString								SecondaryIdentifierKey;
		TMap<FString, STreeConfigItem>		Trees;
	};

	struct STreeMeshData
	{
		UStaticMesh			*Mesh = 0;
		FTransform			Transform;
	};

public:	

	static TObjectPtr<AOCGFeaturesTreeMeshVisualiser> Create(UWorld *World, const FJsonObject &Parameters);

	virtual void SetFeaturesCollection(const GenericTwin::OGCFeaturesCollection &FeaturesCollection, AGeoReferencingSystem &GeoReferencingSystem, const FVector &RefWorldLocation) override;

	virtual uint32 GetEstimatedGPUMemoryUsage() const override;

protected:

	static void LoadConfiguration(const FString &ConfigurationFile);
	static void LoadTreeTypeMappings(const FString &MappingFile);


	void CalculateTreePlacement(const GenericTwin::OGCFeaturesCollection &FeaturesCollection, AGeoReferencingSystem &GeoReferencingSystem, const FVector &RefWorldLocation);
	void OnMeshesLoaded(TArray<FString> MeshReferences);
	void PlaceTrees();

	FString GetTree(const FString &PrimaryId, float TrunkRadius, float CrownRadius, FTransform &Transform);

	UStaticMesh* GetTreeMesh(const FString &PrimaryId, float TrunkRadius, float CrownRadius, float &Scale);

	AOCGFeaturesTreeMeshVisualiser();

	UPROPERTY()
	USceneComponent							*Root = 0;

	UPROPERTY()
	TArray<UStaticMeshComponent*>			MeshComponents;

	TMap<FString, TArray<FTransform> >		m_TreePlacements;

	TArray<STreeMeshData>					m_PlacedTrees;
	int32									m_NumRemainingTrees = 0;

	TSharedPtr<struct FStreamableHandle >	m_AsyncHandle;

	uint32									m_EstimatedGPUUsage = 0;

	static STreesConfiguration				s_TreesConfiguration;

	static TMap<FString, FString>			s_TreeTypeMappings;

};
