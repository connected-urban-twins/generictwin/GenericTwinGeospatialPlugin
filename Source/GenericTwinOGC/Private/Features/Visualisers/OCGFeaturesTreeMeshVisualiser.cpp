/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PgattungICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "Features/Visualisers/OCGFeaturesTreeMeshVisualiser.h"

#include "Common/JobQueue/GenericTwinJobBase.h"
#include "Common/JobQueue/GenericTwinJobDispatcher.h"

#include "Math/UnrealMathUtility.h"

#include "Utils/FilePathUtils.h"

#include "Serialization/JsonReader.h"
#include "Serialization/JsonSerializer.h"

#include "GeoReferencingSystem.h"

#include "Engine/StreamableManager.h"
#include "Engine/AssetManager.h"

DEFINE_LOG_CATEGORY(LogOCGFeaturesTreeMeshVisualiser);

AOCGFeaturesTreeMeshVisualiser::STreesConfiguration AOCGFeaturesTreeMeshVisualiser::s_TreesConfiguration;
TMap<FString, FString> AOCGFeaturesTreeMeshVisualiser::s_TreeTypeMappings;

TObjectPtr<AOCGFeaturesTreeMeshVisualiser> AOCGFeaturesTreeMeshVisualiser::Create(UWorld *World, const FJsonObject &Parameters)
{
	AOCGFeaturesTreeMeshVisualiser *visualiser = 0;
	if	(	Parameters.HasTypedField<EJson::String>(TEXT("tree_mesh_config"))
		&&	Parameters.HasTypedField<EJson::String>(TEXT("primary_identifier_key"))
		)
	{
		visualiser = World->SpawnActor<AOCGFeaturesTreeMeshVisualiser>(FVector::ZeroVector, FRotator::ZeroRotator, FActorSpawnParameters());

		if(visualiser)
		{
			if(s_TreesConfiguration.IsLoaded == false)
			{
				LoadConfiguration( GenericTwin::FPathUtils::MakeAbsolutPath(Parameters.GetStringField(TEXT("tree_mesh_config")), FPaths::LaunchDir()));
				s_TreesConfiguration.IsLoaded = true;
				s_TreesConfiguration.PrimaryIdentifierKey = Parameters.GetStringField(TEXT("primary_identifier_key"));

				if(Parameters.HasTypedField<EJson::String>(TEXT("tree_type_mappings")))
					LoadTreeTypeMappings(GenericTwin::FPathUtils::MakeAbsolutPath(Parameters.GetStringField(TEXT("tree_type_mappings")), FPaths::LaunchDir()));

				if(Parameters.HasTypedField<EJson::String>(TEXT("secondary_identifier_key")))
					s_TreesConfiguration.SecondaryIdentifierKey = Parameters.GetStringField(TEXT("secondary_identifier_key"));
			}
		}
	}

	return visualiser;
}


AOCGFeaturesTreeMeshVisualiser::AOCGFeaturesTreeMeshVisualiser()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<UGenericTwinCustomMeshComponent>(TEXT("USceneComponent"));
	Root->SetMobility(EComponentMobility::Movable);

	SetRootComponent(Root);
}

/*

	extract location and mesh ref for each tree
	collect all mesh types for this set

	schedule job to perform line trace for each location
	schedule job to game thread to finally create static mesh cmps and place trees, possible in multiple ticks

*/

void AOCGFeaturesTreeMeshVisualiser::SetFeaturesCollection(const GenericTwin::OGCFeaturesCollection &FeaturesCollection, AGeoReferencingSystem &GeoReferencingSystem, const FVector &RefWorldLocation)
{
	class CalculateTreePlacementJob	:	public GenericTwin::JobBase
	{
	public:

		CalculateTreePlacementJob(AOCGFeaturesTreeMeshVisualiser &TreeVisualiser, const GenericTwin::OGCFeaturesCollection &FeaturesCollection, AGeoReferencingSystem &GeoReferencingSystem, const FVector &RefWorldLocation)
			:	JobBase(GenericTwin::EJobType::Background)
			,	m_TreeVisualiser(TreeVisualiser)
			,	m_FeaturesCollection(FeaturesCollection)
			,	m_GeoReferencingSystem(GeoReferencingSystem)
			,	m_RefWorldLocation(RefWorldLocation)
		{
		}

		virtual GenericTwin::TJobBasePtr Execute() override
		{
			m_TreeVisualiser.CalculateTreePlacement(m_FeaturesCollection, m_GeoReferencingSystem, m_RefWorldLocation);
			return GenericTwin::TJobBasePtr();
		}

		virtual void OnJobFinished()
		{
			if(m_TreeVisualiser.m_TreePlacements.Num() > 0)
			{
				FStreamableManager& StreamableMgr = UAssetManager::GetStreamableManager();
				TArray<FString> meshReferences;
				m_TreeVisualiser.m_TreePlacements.GetKeys(meshReferences);
				TArray<FSoftObjectPath> pathes;
				for(const auto &meshRef : meshReferences)
					pathes.Add(meshRef);

				m_TreeVisualiser.m_AsyncHandle = StreamableMgr.RequestAsyncLoad(pathes, FStreamableDelegate::CreateUObject(&m_TreeVisualiser, &AOCGFeaturesTreeMeshVisualiser::OnMeshesLoaded, meshReferences));
			}
		}

	private:

		AOCGFeaturesTreeMeshVisualiser			&m_TreeVisualiser;
		GenericTwin::OGCFeaturesCollection		m_FeaturesCollection;
		AGeoReferencingSystem					&m_GeoReferencingSystem;
		FVector									m_RefWorldLocation;
	};

	FGenericTwinJobDispatcher::DispatchJob(GenericTwin::TJobBasePtr( new CalculateTreePlacementJob(*this, FeaturesCollection, GeoReferencingSystem, RefWorldLocation) ));

}

void AOCGFeaturesTreeMeshVisualiser::OnMeshesLoaded(TArray<FString> MeshReferences)
{
	TArray< UObject* > loadedAssets;
	m_AsyncHandle->GetLoadedAssets(loadedAssets);
	UE_LOG(LogOCGFeaturesTreeMeshVisualiser, Log, TEXT("Loaded %d assets, %d assets were required"), loadedAssets.Num(), MeshReferences.Num());

	for(int32 i = FMath::Min(loadedAssets.Num(), MeshReferences.Num()) - 1; i >= 0; --i)
	{
		const FString &meshRef = MeshReferences[i];
		UStaticMesh *mesh = Cast<UStaticMesh>(loadedAssets[i]);
		if(mesh && m_TreePlacements.Contains(meshRef))
		{
			for(const auto &t : m_TreePlacements[meshRef])
				m_PlacedTrees.Add( {mesh, t} );
		}
		else
		{
			UE_LOG(LogOCGFeaturesTreeMeshVisualiser, Warning, TEXT("Obviously %s couldn't be loaded"), *meshRef);
		}
	}

	m_NumRemainingTrees = m_PlacedTrees.Num();
	UE_LOG(LogOCGFeaturesTreeMeshVisualiser, Log, TEXT("Collected %d tree meshes for placing into to scene"), m_NumRemainingTrees);

	if(m_NumRemainingTrees > 0)
	{
		--m_NumRemainingTrees;
		PlaceTrees();
	}
}

void AOCGFeaturesTreeMeshVisualiser::PlaceTrees()
{
	class PlaceTreesJob	:	public GenericTwin::JobBase
	{
	public:

		PlaceTreesJob(AOCGFeaturesTreeMeshVisualiser &TreeVisualiser)
			:	JobBase(GenericTwin::EJobType::GameThread)
			,	m_TreeVisualiser(TreeVisualiser)
		{
		}

		virtual GenericTwin::TJobBasePtr Execute() override
		{
			const int32 MaxTreesPerCycle = 16;
			for(int32 i = 0; i < MaxTreesPerCycle && m_TreeVisualiser.m_NumRemainingTrees >= 0; ++i, --m_TreeVisualiser.m_NumRemainingTrees)
			{
				UStaticMeshComponent *treeMeshCmp = NewObject<UStaticMeshComponent>(&m_TreeVisualiser, UStaticMeshComponent::StaticClass());
				if(treeMeshCmp)
				{
					treeMeshCmp->RegisterComponent();
					treeMeshCmp->SetRelativeTransform(m_TreeVisualiser.m_PlacedTrees[m_TreeVisualiser.m_NumRemainingTrees].Transform);
					treeMeshCmp->AttachToComponent(m_TreeVisualiser.Root, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));

					treeMeshCmp->SetStaticMesh(m_TreeVisualiser.m_PlacedTrees[m_TreeVisualiser.m_NumRemainingTrees].Mesh);
					m_TreeVisualiser.MeshComponents.Add(treeMeshCmp);
				}
			}

			return m_TreeVisualiser.m_NumRemainingTrees < 0 ? GenericTwin::TJobBasePtr() : GenericTwin::TJobBasePtr( new PlaceTreesJob(m_TreeVisualiser) );
		}

	private:
		AOCGFeaturesTreeMeshVisualiser			&m_TreeVisualiser;
	};

	FGenericTwinJobDispatcher::DispatchJob(GenericTwin::TJobBasePtr( new PlaceTreesJob(*this) ));

}

void AOCGFeaturesTreeMeshVisualiser::CalculateTreePlacement(const GenericTwin::OGCFeaturesCollection &FeaturesCollection, AGeoReferencingSystem &GeoReferencingSystem, const FVector &RefWorldLocation)
{
	const float MinTrunkRadius = 10.0;
	const float MinCrownRadius = 25.0;

	UE_LOG(LogOCGFeaturesTreeMeshVisualiser, Log, TEXT("Adding trees at %s"), *(RefWorldLocation.ToString()));

	uint32 numTrees = 0;
	for(const GenericTwin::OGCFeaturesItem &feature : FeaturesCollection.GetFeatures())
	{
		if	(	feature.GeometryType == GenericTwin::OGCFeaturesItem::Points
			&&	feature.Coordinates.Num() > 0
			&&	feature.Properties.Contains(TEXT("stammumfang"))
			&&	feature.Properties.Contains(TEXT("kronendurchmesser"))
			)
		{
			const float trunkRadius = FCString::Atof(*(feature.Properties[TEXT("stammumfang")])) / (2.0 * PI);
			const float crownRadius = FCString::Atof(*(feature.Properties[TEXT("kronendurchmesser")])) * 50.0;	// scale to cm and divide by 2

			if	(	feature.Properties.Contains(s_TreesConfiguration.PrimaryIdentifierKey)
				&&	trunkRadius > MinTrunkRadius
				&&	crownRadius > MinCrownRadius
				)
			{
				FString primaryId = feature.Properties[s_TreesConfiguration.PrimaryIdentifierKey].ToLower();
				if	(	s_TreesConfiguration.Trees.Contains(primaryId) == false
					&&	s_TreeTypeMappings.Contains(primaryId)
					)
				{
					primaryId = s_TreeTypeMappings[primaryId];
				}

				if(s_TreesConfiguration.Trees.Contains(primaryId))
				{
					FTransform transform;
					FString meshRef = GetTree(primaryId, trunkRadius, crownRadius, transform);

					if(meshRef.IsEmpty() == false)
					{
						FVector projectedCoord(feature.Coordinates[0], 0.0);
						FVector treeLocation;
						GeoReferencingSystem.ProjectedToEngine(projectedCoord, treeLocation);
						
						double height = 0.0;
						GetGroundHeight(treeLocation, height);

						treeLocation -= RefWorldLocation;
						treeLocation.Z = height;
						transform.SetLocation(treeLocation);

						if(m_TreePlacements.Contains(meshRef) == false)
						{
							m_TreePlacements.Add(meshRef, TArray<FTransform>() );
						}

						m_TreePlacements[meshRef].Add(transform);
						++numTrees;
					}
				}
			}
		}
	}

	UE_LOG(LogOCGFeaturesTreeMeshVisualiser, Log, TEXT("%d trees added"), numTrees);
}

FString AOCGFeaturesTreeMeshVisualiser::GetTree(const FString &PrimaryId, float TrunkRadius, float CrownRadius, FTransform &Transform)
{
	const STreeConfigItem &tci = s_TreesConfiguration.Trees[PrimaryId];

	int32 bestInd = -1;
	float bestCrownDelta = TNumericLimits<float>::Max();
	for(int32 i = 0; i < tci.Meshes.Num(); ++i)
	{
		const float curCrownDelta = FMath::Abs(1.0f - CrownRadius / tci.Meshes[i].CrownRadius);
		if(curCrownDelta < bestCrownDelta)
		{
			bestCrownDelta = curCrownDelta;
			bestInd = i;
		}
	}

	FString meshRef;
	if(bestInd >= 0)
	{
		meshRef = tci.Meshes[bestInd].MeshReference;
		const float scale = CrownRadius / tci.Meshes[bestInd].CrownRadius;
		Transform.SetScale3D(FVector(scale, scale, scale * FMath::FRandRange(0.95f, 1.05f)));
		Transform.SetRotation(FRotator(0.0, FMath::RandRange(-180.0, 180.0), 0.0).Quaternion());
	}

	return meshRef;
}

UStaticMesh* AOCGFeaturesTreeMeshVisualiser::GetTreeMesh(const FString &PrimaryId, float TrunkRadius, float CrownRadius, float &Scale)
{
	UStaticMesh *mesh = 0;

	const STreeConfigItem &tci = s_TreesConfiguration.Trees[PrimaryId];

	int32 bestInd = -1;
	float bestCrownDelta = TNumericLimits<float>::Max();
	for(int32 i = 0; i < tci.Meshes.Num(); ++i)
	{
		const float curCrownDelta = FMath::Abs(1.0f - CrownRadius / tci.Meshes[i].CrownRadius);
		if(curCrownDelta < bestCrownDelta)
		{
			bestCrownDelta = curCrownDelta;
			bestInd = i;
		}
	}

	if(bestInd >= 0)
	{
		const FString &meshRef = tci.Meshes[bestInd].MeshReference;
		mesh = Cast<UStaticMesh>(StaticLoadObject(UStaticMesh::StaticClass(), this, *meshRef));
		Scale = CrownRadius / 1060.0;// /tci.Meshes[bestInd].CrownRadius;
	}

	return mesh;
}

void AOCGFeaturesTreeMeshVisualiser::LoadConfiguration(const FString &ConfigurationFile)
{
	FString jsonStr;
	if (FFileHelper::LoadFileToString(jsonStr, *ConfigurationFile))
	{
		UE_LOG(LogOCGFeaturesTreeMeshVisualiser, Log, TEXT("Successfully opened configuration file %s"), *ConfigurationFile);
		TSharedRef<TJsonReader<>> reader = TJsonReaderFactory<>::Create(jsonStr);

		TSharedPtr<FJsonObject> jsonObject;
		if (FJsonSerializer::Deserialize(reader, jsonObject))
		{
			UE_LOG(LogOCGFeaturesTreeMeshVisualiser, Log, TEXT("Successfully deserialized json config file"));

			const TArray< TSharedPtr< FJsonValue > > *treesArray = 0;
			if	(	jsonObject->TryGetArrayField(TEXT("trees"), treesArray)
				&&	treesArray
				)
			{
				for(const TSharedPtr<FJsonValue> &treeVal : (*treesArray))
				{
					if(treeVal->Type == EJson::Object)
					{
						const TSharedPtr<FJsonObject> &treeObject = treeVal->AsObject();

						const TArray< TSharedPtr< FJsonValue > > *meshesArray = 0;
						if	(	treeObject->HasTypedField<EJson::String>(TEXT("primary_type"))
							&&	treeObject->TryGetArrayField(TEXT("meshes"), meshesArray)
							&&	meshesArray
							)
						{
							STreeConfigItem treeCfgItem;

							for(const TSharedPtr<FJsonValue> &meshVal : (*meshesArray))
							{
								if(meshVal->Type == EJson::Object)
								{
									const TSharedPtr<FJsonObject> &meshObject = meshVal->AsObject();
									if(meshObject->HasTypedField<EJson::String>(TEXT("mesh")))
									{
										STreeMeshConfigItem meshCfgItem;

										meshCfgItem.MeshReference = meshObject->GetStringField(TEXT("mesh"));

										if(meshObject->HasTypedField<EJson::Number>(TEXT("trunk_radius")))
											meshCfgItem.TrunkRadius = meshObject->GetNumberField(TEXT("trunk_radius"));
										if(meshObject->HasTypedField<EJson::Number>(TEXT("crown_radius")))
											meshCfgItem.CrownRadius = meshObject->GetNumberField(TEXT("crown_radius"));
										if(meshObject->HasTypedField<EJson::Number>(TEXT("tree_height")))
											meshCfgItem.TreeHeight = meshObject->GetNumberField(TEXT("tree_height"));
										
										treeCfgItem.Meshes.Add(meshCfgItem);

									}
								}
							}
	
							if(treeCfgItem.Meshes.Num() > 0)
								s_TreesConfiguration.Trees.Add(treeObject->GetStringField(TEXT("primary_type")), treeCfgItem);

						}

					}
				}
			}
		}
		else
			UE_LOG(LogOCGFeaturesTreeMeshVisualiser, Error, TEXT("Not a valid json file"));
	}
	else
	{
		UE_LOG(LogOCGFeaturesTreeMeshVisualiser, Warning, TEXT("File %s not found"), *ConfigurationFile);
	}
}

void AOCGFeaturesTreeMeshVisualiser::LoadTreeTypeMappings(const FString &MappingFile)
{
	TArray<FString> lines;
	if(FFileHelper::LoadFileToStringArray(lines, *MappingFile))
	{
		if(lines.Num() > 0)
		{
			const FString delimiter(TEXT(","));
			TArray<FString> parts;
			for(int32 i = 0; i < lines.Num(); ++i)
			{
				lines[i].ParseIntoArray(parts, *delimiter, false);

				if(parts.Num() == 2)
				{
					FString typeStr = parts[1].ToLower();

					if	(	typeStr == TEXT("broadleaf")
						||	typeStr == TEXT("conifer")
						)
					{
						s_TreeTypeMappings.Add(parts[0].ToLower(), typeStr);
					}
				}
			}
		}
		else
		{
			UE_LOG(LogOCGFeaturesGenericTreeVisualiser, Warning, TEXT("No lines found"));
		}
	}
	else
	{
		UE_LOG(LogOCGFeaturesGenericTreeVisualiser, Warning, TEXT("File %s not found"), *MappingFile);
	}
}

uint32 AOCGFeaturesTreeMeshVisualiser::GetEstimatedGPUMemoryUsage() const
{
	return m_EstimatedGPUUsage;
}

#if 0


void AOCGFeaturesTreeMeshVisualiser::SetFeaturesCollection(const GenericTwin::OGCFeaturesCollection &FeaturesCollection, AGeoReferencingSystem &GeoReferencingSystem, const FVector &RefWorldLocation)
{
	const float MinTrunkRadius = 10.0;
	const float MinCrownRadius = 25.0;

	UE_LOG(LogOCGFeaturesTreeMeshVisualiser, Log, TEXT("Adding tree meshes at %s"), *(RefWorldLocation.ToString()));

	for(const GenericTwin::OGCFeaturesItem &feature : FeaturesCollection.GetFeatures())
	{
		if	(	feature.GeometryType == GenericTwin::OGCFeaturesItem::Points
			&&	feature.Coordinates.Num() > 0
			&&	feature.Properties.Contains(TEXT("stammumfang"))
			&&	feature.Properties.Contains(TEXT("kronendurchmesser"))
			)
		{
			const float trunkRadius = FCString::Atof(*(feature.Properties[TEXT("stammumfang")])) / (2.0 * PI);
			const float crownRadius = FCString::Atof(*(feature.Properties[TEXT("kronendurchmesser")])) * 50.0;	// scale to cm and divide by 2

			if	(	feature.Properties.Contains(s_TreesConfiguration.PrimaryIdentifierKey)
				&&	trunkRadius > MinTrunkRadius
				&&	crownRadius > MinCrownRadius
				)
			{
				FString primaryId = feature.Properties[s_TreesConfiguration.PrimaryIdentifierKey].ToLower();
				if	(	s_TreesConfiguration.Trees.Contains(primaryId) == false
					&&	s_TreeTypeMappings.Contains(primaryId)
					)
				{
					primaryId = s_TreeTypeMappings[primaryId];
				}

				if(s_TreesConfiguration.Trees.Contains(primaryId))
				{
					FVector projectedCoord(feature.Coordinates[0], 0.0);
					FVector treeLocation;
					GeoReferencingSystem.ProjectedToEngine(projectedCoord, treeLocation);
					
					// do line trace
					double height = 0.0;
					GetGroundHeight(treeLocation, height);

					treeLocation -= RefWorldLocation;

					treeLocation.Z = height;

					float scale = 1.0f;
					UStaticMesh *mesh = GetTreeMesh(primaryId, trunkRadius, crownRadius, scale);

					if(mesh)
					{
						UStaticMeshComponent *treeMeshCmp = NewObject<UStaticMeshComponent>(this, UStaticMeshComponent::StaticClass());
						if(treeMeshCmp)
						{
							treeMeshCmp->RegisterComponent();
							treeMeshCmp->SetRelativeLocation(treeLocation); 
							treeMeshCmp->SetRelativeRotation(FRotator(0.0, FMath::RandRange(-180.0, 180.0), 0.0));
							treeMeshCmp->SetRelativeScale3D(FVector(scale, scale, FMath::FRandRange(0.95f, 1.05f) * scale));
							treeMeshCmp->AttachToComponent(Root, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));

							treeMeshCmp->SetStaticMesh(mesh);
							MeshComponents.Add(treeMeshCmp);
						}
					}
				}
			}
		}
	}

	UE_LOG(LogOCGFeaturesTreeMeshVisualiser, Log, TEXT("%d tree meshes added"), MeshComponents.Num());
}

#endif
