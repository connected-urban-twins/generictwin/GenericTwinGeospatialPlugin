/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "Features/GenericTwinOGCFeaturesVisualiser.h"

#include "OCGFeaturesGenericTreeVisualiser.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogOCGFeaturesGenericTreeVisualiser, Log, All);

namespace GenericTwin {
struct SMeshSection;
class IStaticMeshComponent;
}	//	namespace GenericTwin

class UGenericTwinCustomMeshComponent;

UCLASS()
class GENERICTWINOGC_API AOCGFeaturesGenericTreeVisualiser	:	public AGenericTwinOGCFeaturesVisualiser
{
	GENERATED_BODY()

	enum ETreeType
	{
		Undefined,
		Broadleaf,
		Conifer
	};

	struct STrunkParameters
	{
		float					Radius = 0.0f;			// radius of trunk in m, typically measured at 130 cm from ground

		// simple exponentional formula (Base ^ [rel] Height) for trunk widening/tapering
		float					WideningBase = 2.0f;
		float					TaperingBase = 0.975f;
		float 					TaperingPerHeight = 400.0f;

		float					TexSizeT = 200.0f;

		int32					NumSegments = 12;
		int32					NumBaseSlices = 5;		// number of slices from fround to 130 cm of height, where the trunk is widening
		int32					NumSlices = 2;			// number of slices from 130 cm above ground to the top
	};

	struct SBroadleafParameters
	{
		float					Radius = 0.0f;			// radius of crown in m
		int32					NumSegments = 40;
		int32					NumSlices = 30;

		FVector2D				Flattening = FVector2D(0.9f, 1.3f);
	};


	struct SConiferParameters
	{
		float					Radius = 0.0f;			// radius of crown in m
		float					Height = 0.0f;			// height of conifer crown

		int32					NumSegments = 20;
		int32					NumSlices = 4;

		FVector2D				RadiusReduction = FVector2D(0.7f, 0.8f);

		FVector2D				ConeFlattening = FVector2D(0.85f, 0.95f);
		FVector2D				ConeSize = FVector2D(1.05f, 1.25f);
		FVector2D				ConeOffset = FVector2D(0.75f, 0.85f);
	};

	struct STreeConfigItem
	{
		FString					SecondaryIdentifier;

		ETreeType				TreeType;
	};

	struct STreesConfiguration
	{
		FString								PrimaryIdentifierKey;
		FString								SecondaryIdentifierKey;
		TMap<FString, STreeConfigItem>		Trees;
	};

public:	

	static TObjectPtr<AOCGFeaturesGenericTreeVisualiser> Create(UWorld *World, const FJsonObject &Parameters);

	virtual void SetFeaturesCollection(const GenericTwin::OGCFeaturesCollection &FeaturesCollection, AGeoReferencingSystem &GeoReferencingSystem, const FVector &RefWorldLocation) override;

	virtual uint32 GetEstimatedGPUMemoryUsage() const override;

protected:

	void BuildTreeGeometry(const GenericTwin::OGCFeaturesCollection &FeaturesCollection, AGeoReferencingSystem &GeoReferencingSystem, const FVector &RefWorldLocation);
	void UploadGeometry();

	void CreateTrunk(GenericTwin::SMeshSection &MeshSection, const FVector &TreeLocation, double TrunkHeight, const STrunkParameters &Parameters);

	void CreateBroadleafCrown(GenericTwin::SMeshSection &MeshSection, const FVector &TreeLocation, double HeightOffset, const SBroadleafParameters &Parameters);

	void CreateConiferCrown(GenericTwin::SMeshSection &MeshSection, const FVector &TreeLocation, double HeightOffset, const SConiferParameters&Parameters);

	void CalculateNormals(GenericTwin::SMeshSection &MeshSection);

	TArray<FVector> CreateSlice(int32 NumSegments);

	static void LoadConfiguration(const FString &ConfigurationFile);

	AOCGFeaturesGenericTreeVisualiser();

	// UPROPERTY()
	// UGenericTwinCustomMeshComponent			*MeshComponent = 0;

	GenericTwin::IStaticMeshComponent		*MeshComponent = 0;

	GenericTwin::SMeshSection				m_TrunkSection;
	GenericTwin::SMeshSection				m_CrownSection;

	uint32									m_EstimatedGPUUsage = 0;

	static STreesConfiguration				s_TreesConfiguration;

	static UMaterial						*s_TrunkMaterial;
	static UMaterial						*s_CrownMaterial;
};
