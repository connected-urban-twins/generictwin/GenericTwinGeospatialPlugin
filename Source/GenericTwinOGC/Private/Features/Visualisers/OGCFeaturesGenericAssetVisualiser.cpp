/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "Features/Visualisers/OGCFeaturesGenericAssetVisualiser.h"
#include "Features/Actors/GenericTwinOGCPointFeature.h"


#include "Utils/GenericTwinClassFinder.h"


TObjectPtr<AOGCFeaturesGenericAssetVisualiser> AOGCFeaturesGenericAssetVisualiser::Create(UWorld *World, const FJsonObject &Parameters)
{
	AOGCFeaturesGenericAssetVisualiser *visualiser = 0;
	if	(	Parameters.HasTypedField<EJson::String>(TEXT("asset_type"))
		&&	Parameters.HasTypedField<EJson::String>(TEXT("asset_reference"))
		)
	{
		visualiser = World->SpawnActor<AOGCFeaturesGenericAssetVisualiser>(FVector::ZeroVector, FRotator::ZeroRotator, FActorSpawnParameters());

		if(visualiser)
		{
			visualiser->m_AssetType = Parameters.GetStringField(TEXT("asset_type"));
			visualiser->m_AssetReference = Parameters.GetStringField(TEXT("asset_reference"));
		}
	}

	return visualiser;
}


AOGCFeaturesGenericAssetVisualiser::AOGCFeaturesGenericAssetVisualiser()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	// PrimaryActorTick.bCanEverTick = true;

}

void AOGCFeaturesGenericAssetVisualiser::SetFeaturesCollection(const GenericTwin::OGCFeaturesCollection &FeaturesCollection, AGeoReferencingSystem &GeoReferencingSystem, const FVector &RefWorldLocation)
{
	UClass *assetClass = UGenericTwinClassFinder::FindClass(GetWorld(), m_AssetReference);
	for(const GenericTwin::OGCFeaturesItem &feature : FeaturesCollection.GetFeatures())
	{
		if	(	(feature.GeometryType == GenericTwin::OGCFeaturesItem::Points || feature.GeometryType == GenericTwin::OGCFeaturesItem::Point)
			&&	feature.Coordinates.Num() > 0
			)
		{
			FVector projectedCoord(feature.Coordinates[0], 0.0);
			FVector location;
			GeoReferencingSystem.ProjectedToEngine(projectedCoord, location);

			// do line trace
			double height = 0.0;
			GetGroundHeight(location, height);
			// location -= RefWorldLocation;
			location.Z = height;

			AActor *actor = GetWorld()->SpawnActor<AActor>(assetClass);
			if(actor)
			{
				actor->SetActorLocation(location);
				m_AssetActors.Add(actor);

				AGenericTwinOGCPointFeature *pfActor = Cast<AGenericTwinOGCPointFeature> (actor);
				if(pfActor)
				{
					pfActor->SetFeatureProperties(feature.Properties);
				}
			}

		}
	}
}

uint32 AOGCFeaturesGenericAssetVisualiser::GetEstimatedGPUMemoryUsage() const
{
	return 0;
}

void AOGCFeaturesGenericAssetVisualiser::SetVisibility(bool IsVisible)
{
	for(AActor *actor : m_AssetActors)
	{
		actor->SetActorHiddenInGame(!IsVisible);
	}
}
