/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "Features/OGCFeaturesCollection.h"

namespace GenericTwin {

OGCFeaturesCollection::OGCFeaturesCollection()
{

}

void OGCFeaturesCollection::LoadFromJson(const FString &JsonString)
{
	TSharedRef<TJsonReader<>> reader = TJsonReaderFactory<>::Create(JsonString);
	TSharedPtr<FJsonObject> jsonObject;
	if (FJsonSerializer::Deserialize(reader, jsonObject))
	{
		if	(	jsonObject->HasTypedField<EJson::String>(TEXT("type"))
			&&	jsonObject->GetStringField(TEXT("type")).Compare(TEXT("FeatureCollection"), ESearchCase::IgnoreCase) == 0
			&&	jsonObject->HasTypedField<EJson::Array>(TEXT("features"))
			)
		{
			const TArray < TSharedPtr < FJsonValue > > &features = jsonObject->GetArrayField(TEXT("features"));
			for(const auto &feature : features)
			{
				const TSharedPtr < FJsonObject > * featureObject = 0;
				if	(	feature->TryGetObject(featureObject)
					&&	featureObject
					&&	(*featureObject)->HasTypedField<EJson::String>(TEXT("type"))
					&&	(*featureObject)->GetStringField(TEXT("type")).Compare(TEXT("Feature")) == 0
					&&	(*featureObject)->HasTypedField<EJson::Object>(TEXT("geometry"))
					)
				{
					OGCFeaturesItem item;

					const TSharedPtr< FJsonObject > &geometryObject = (*featureObject)->GetObjectField(TEXT("geometry"));
					if	(	geometryObject->HasTypedField<EJson::String>(TEXT("type"))
						&&	geometryObject->HasTypedField<EJson::Array>(TEXT("coordinates"))
						)
					{
						OGCFeaturesItem::EGeometryType type = OGCFeaturesCollection::GetGeometryType(geometryObject->GetStringField(TEXT("type")));
						if(type != OGCFeaturesItem::Undefined)
						{
							item.GeometryType = type;
							if(geometryObject->HasTypedField<EJson::String>(TEXT("id")))
								item.ItemId = geometryObject->GetStringField(TEXT("id"));

							const TArray < TSharedPtr < FJsonValue > > &coordinates = geometryObject->GetArrayField(TEXT("coordinates"));
							if(type == OGCFeaturesItem::Point)
							{
								if	(	coordinates.Num() == 2
									&&	coordinates[0]->Type == EJson::Number
									&&	coordinates[1]->Type == EJson::Number
									)
								{
									item.Coordinates.Add(FVector2D(coordinates[0]->AsNumber(), coordinates[1]->AsNumber()));
								}
							}
							else
							{
								for(const auto &coordinate : coordinates)
								{
									if(coordinate->Type == EJson::Array)
									{
										const TArray< TSharedPtr< FJsonValue > > &coordArray = coordinate->AsArray(); 
										if	(	coordArray.Num() == 2
											&&	coordArray[0]->Type == EJson::Number
											&&	coordArray[1]->Type == EJson::Number
											)
										{
											item.Coordinates.Add(FVector2D(coordArray[0]->AsNumber(), coordArray[1]->AsNumber()));
										}
									}

								}
							}
						}
					}

					// extract properties
					const TSharedPtr< FJsonObject > *propertiesObject = 0;
					if	(	item.Coordinates.Num() > 0
						&&	(*featureObject)->TryGetObjectField(TEXT("properties"), propertiesObject)
						&&	propertiesObject
						)
					{
						for(const auto &val : (*propertiesObject)->Values)
						{
							const TSharedPtr<FJsonValue> &valPtr = val.Value;
							if(valPtr->Type == EJson::String)
							{
								item.Properties.Add(val.Key.ToLower(), valPtr->AsString());
							}
							else if(valPtr->Type == EJson::Number)
							{
								item.Properties.Add(val.Key.ToLower(), FString::SanitizeFloat(valPtr->AsNumber()));
							}
						}
					}

					if (item.Coordinates.Num() > 0)
						m_Items.Add(item);
				}
			}

		}
	}

}

OGCFeaturesItem::EGeometryType OGCFeaturesCollection::GetGeometryType(const FString &TypeStr)
{
	OGCFeaturesItem::EGeometryType type = OGCFeaturesItem::Undefined;

	if(TypeStr.Compare(TEXT("Point")) == 0)
		type = OGCFeaturesItem::Point;
	if(TypeStr.Compare(TEXT("MultiPoint")) == 0)
		type = OGCFeaturesItem::Points;
	else if(TypeStr.Compare(TEXT("Polygon")) == 0)
		type = OGCFeaturesItem::Polygon;
	else if(TypeStr.Compare(TEXT("LineString")) == 0)
		type = OGCFeaturesItem::Line;

	return type;
}

}	//	namespace GenericTwin
