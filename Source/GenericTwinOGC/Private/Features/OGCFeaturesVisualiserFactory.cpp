/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "Features/OGCFeaturesVisualiserFactory.h"

namespace GenericTwin {

OGCFeaturesVisualiserFactory *OGCFeaturesVisualiserFactory::s_Instance = 0;

OGCFeaturesVisualiserFactory& OGCFeaturesVisualiserFactory::GetInstance()
{
	if(s_Instance == 0)
	{
		s_Instance = new OGCFeaturesVisualiserFactory;
	}

	return *s_Instance;
}

OGCFeaturesVisualiserFactory::OGCFeaturesVisualiserFactory()
{
}

OGCFeaturesVisualiserFactory::~OGCFeaturesVisualiserFactory()
{
}

void OGCFeaturesVisualiserFactory::RegisterVisualiser(const FString &Type, CreateVisualiserFuncPtr CreateFuncPtr)
{
	if(m_RegisteredVisualisers.Contains(Type) == false)
	{
		m_RegisteredVisualisers.Add(Type, CreateFuncPtr);
	}
}


TObjectPtr<AGenericTwinOGCFeaturesVisualiser> OGCFeaturesVisualiserFactory::CreateVisualiser(UWorld *World, const FString &Type, const FJsonObject &Parameters) const
{
	return m_RegisteredVisualisers.Contains(Type) ? m_RegisteredVisualisers[Type](World, Parameters) : 0;
}


}	//	namespace GenericTwin
