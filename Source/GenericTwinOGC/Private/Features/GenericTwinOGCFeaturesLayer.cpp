/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "Features/GenericTwinOGCFeaturesLayer.h"

#include "Features/GenericTwinOGCFeaturesTile.h"
#include "Features/OGCFeaturesVisualiserFactory.h"

#include "TwinWorldContext.h"
#include "Common/Configuration/ConfigurationJsonBuilder.h"

#include "SpatialPartitioning/TileManager/TilingHelpers.h"

#include "Serialization/JsonReader.h"
#include "Serialization/JsonSerializer.h"

#include "GeoReferencingSystem.h"

DEFINE_LOG_CATEGORY(LogGenericTwinOGCFeaturesLayer);

const SLayerMetaData& UGenericTwinOGCFeaturesLayer::GetLayerMetaData()
{
	static SLayerMetaData layerMetaData =	{	TEXT("OGCFeatures")
											,	FText::FromString(TEXT("OGC Features API"))
											,	FText::FromString(TEXT("OGC Features API"))
											,	ELayerContributionType::Visualisation
											,	ELayerCreationType::RuntimeMultiple
											};

	return layerMetaData;
}

TSharedPtr<FJsonObject> UGenericTwinOGCFeaturesLayer::GetDefaultConfiguration()
{
	static TSharedPtr<FJsonObject> defaultConfig;

	if(!defaultConfig)
	{
		ConfigurationJsonBuilder jcb;
		jcb.Begin();
		defaultConfig = jcb.Finalize();
	}

	return defaultConfig;
}

ULayerBase* UGenericTwinOGCFeaturesLayer::Create(FTwinWorldContext &TwinWorldCtx, const TSharedPtr<FJsonObject> &Parameters)
{
	UGenericTwinOGCFeaturesLayer *layer = NewObject<UGenericTwinOGCFeaturesLayer>(TwinWorldCtx.World, FName(), RF_Standalone);

	if(layer)
	{
		layer->OnConstruction(GetLayerMetaData(), ELayerPriority::Medium);
		layer->ExtractParameters(*Parameters, TwinWorldCtx);
	}

	return layer;
}

UGenericTwinOGCFeaturesLayer::UGenericTwinOGCFeaturesLayer()
{
}

UGenericTwinOGCFeaturesLayer::~UGenericTwinOGCFeaturesLayer()
{
}

void UGenericTwinOGCFeaturesLayer::ExtractParameters(const FJsonObject &Parameters, FTwinWorldContext &TwinWorldCtx)
{
	if	(	Parameters.HasTypedField<EJson::String>(TEXT("domain"))
		&&	Parameters.HasTypedField<EJson::String>(TEXT("base_url"))
		&&	Parameters.HasTypedField<EJson::Number>(TEXT("tile_size"))
		&&	Parameters.HasTypedField<EJson::Array>(TEXT("crs_reference_location"))
		&&	Parameters.HasTypedField<EJson::Object>(TEXT("visualiser"))
		&&	ExtractVisualiser( Parameters.GetObjectField(TEXT("visualiser")) )
		)
	{
		UTileManager::SConfiguration config;
		config.MaxViewDistance = (Parameters.HasTypedField<EJson::Number>(TEXT("view_distance")) ? Parameters.GetNumberField(TEXT("view_distance")) : 2000.0f) * 100.0;
		config.MaxLoadDistance = Parameters.HasTypedField<EJson::Number>(TEXT("load_distance")) ? (Parameters.GetNumberField(TEXT("load_distance")) * 100.0) : config.MaxViewDistance;
		config.ViewRectHalfSize = Parameters.HasTypedField<EJson::Number>(TEXT("view_rect_half_size")) ? Parameters.GetNumberField(TEXT("view_rect_half_size")) : 4;
		config.MaxHeapMemoroyUsage = (Parameters.HasTypedField<EJson::Number>(TEXT("max_heap_memory_mb")) ? Parameters.GetNumberField(TEXT("max_heap_memory_mb")) : 500) * 1024 * 1024;
		config.MaxGPUMemoryUsage = (Parameters.HasTypedField<EJson::Number>(TEXT("max_gpu_memory_mb")) ? Parameters.GetNumberField(TEXT("max_gpu_memory_mb")) : 500) * 1024 * 1024;
		config.PurgeInterval = Parameters.HasTypedField<EJson::Number>(TEXT("purge_interval_s")) ? Parameters.GetNumberField(TEXT("purge_interval_s")) : 120;

		ExtractStreamingParameters(Parameters, config);
		ConfigureTileManager(TEXT("OGCFeatures"), config);

		m_Domain = Parameters.GetStringField(TEXT("domain"));
		m_BaseUrl = Parameters.GetStringField(TEXT("base_url"));
		if(Parameters.HasTypedField<EJson::String>(TEXT("request_crs")))
		{
			m_RequestCRS = Parameters.GetStringField(TEXT("request_crs"));
		}
		UE_LOG(LogGenericTwinOGCFeaturesLayer, Log, TEXT("RequestCRS: %s"), *m_RequestCRS );

		if(Parameters.HasTypedField<EJson::Number>(TEXT("tile_offset_m")))
		{
			m_TileOffset = Parameters.GetNumberField(TEXT("tile_offset_m")) * 100.0;
		}


		m_TileSizeM = Parameters.GetNumberField(TEXT("tile_size"));
		m_TileSize = FVector2D(100.0 * m_TileSizeM) ;
		const TArray<TSharedPtr<FJsonValue>> arrayVals = Parameters.GetArrayField("crs_reference_location");
		if(arrayVals.Num() == 3)
		{
			TwinWorldCtx.GeoReferencingSystem->ProjectedToEngine(FVector(arrayVals[0]->AsNumber(), arrayVals[1]->AsNumber(), arrayVals[2]->AsNumber()), m_ReferenceLocation);
			m_LocationMapper = new GenericTwin::BasicLocationMapper(m_ReferenceLocation, m_TileSize);
			if(m_LocationMapper)
			{
				SetupTiling(*m_LocationMapper, 1);
			}

		}
	}
}

bool UGenericTwinOGCFeaturesLayer::ExtractVisualiser(const TSharedPtr<FJsonObject> &VisualiserObject)
{
	bool res = false;
	if(VisualiserObject->HasTypedField<EJson::String>(TEXT("type")))
	{
		m_VisualiserType = VisualiserObject->GetStringField(TEXT("type"));
		m_VisualiserObject = *VisualiserObject;

		res = true;
	}
	return res;
}

void UGenericTwinOGCFeaturesLayer::InitializeLayer(FTwinWorldContext &TwinWorldCtx)
{
	m_GeoReferencingSystem = TwinWorldCtx.GeoReferencingSystem;
	SetDependency(m_Dependency);
}

void UGenericTwinOGCFeaturesLayer::UpdateView(FTwinWorldContext &TwinWorldCtx, float DeltaSeconds)
{
	Super::UpdateView(TwinWorldCtx, DeltaSeconds);
}


TileBasePtr UGenericTwinOGCFeaturesLayer::CreateTile(FTwinWorldContext &TwinWorldCtx, const GenericTwin::TTileId &TileId)
{
	UGenericTwinOGCFeaturesTile *tile = 0;
	
	if(m_GeoReferencingSystem)
	{
		tile = NewObject<UGenericTwinOGCFeaturesTile> (TwinWorldCtx.World, FName(), RF_Standalone);

		if(tile)
		{
			tile->SetLayer(this);
			tile->SetTileId(TileId);

			FVector tileLocation = TileId.ToLocation(m_ReferenceLocation, m_TileSize);
			tile->Initialize(tileLocation + FVector(0.0, 0.0, m_TileOffset), m_TileSize);

			FVector locationCRS;
			TwinWorldCtx.GeoReferencingSystem->EngineToProjected(tileLocation, locationCRS);
			tile->SetLocationCRS(locationCRS);

		}
	}

	return TileBasePtr(tile);
}

AGenericTwinOGCFeaturesVisualiser* UGenericTwinOGCFeaturesLayer::CreateVisualiser()
{
	return GenericTwin::OGCFeaturesVisualiserFactory::GetInstance().CreateVisualiser(GetWorld(), m_VisualiserType, m_VisualiserObject);
}

