/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#include "Features/GenericTwinOGCFeaturesTile.h"
#include "Features/GenericTwinOGCFeaturesLayer.h"

#include "Common/Communication/GenericTwinHttpRequestManager.h"

#include "Common/JobQueue/GenericTwinJobBase.h"
#include "Common/JobQueue/GenericTwinJobDispatcher.h"

DEFINE_LOG_CATEGORY(LogGenericTwinOGCFeaturesTile);

UGenericTwinOGCFeaturesTile::UGenericTwinOGCFeaturesTile()
{
}

UGenericTwinOGCFeaturesTile::~UGenericTwinOGCFeaturesTile()
{
}

bool UGenericTwinOGCFeaturesTile::Load()
{
	FString path = BuildRequestUrl(m_LocationCRS);

	FGenericTwinHttpRequestManager &httpReqMgr = FGenericTwinHttpRequestManager::GetInstance();

	const bool asCRS84 = !m_Layer->GetRequestCRS().IsEmpty();

	const int32 reqId = httpReqMgr.CreateRequest	(	m_Layer->GetDomain(), m_Layer->GetBaseUrl(), FGenericTwinHttpRequestManager::GET
													, 	{ {TEXT("Content-Type"), TEXT("*/*")}, {TEXT("accept"), TEXT("application/geo+json")} }
													,	GetParameters(m_LocationCRS, asCRS84)
													);
	if(httpReqMgr.SendRequest(reqId, FOnGenericTwinHttpResponse::CreateUObject(this, &UGenericTwinOGCFeaturesTile::OnResponse), FOnGenericTwinHttpRequestError::CreateUObject(this, &UGenericTwinOGCFeaturesTile::OnRequestError)))
	{
		m_TileLoadState = ETileLoadState::Loading;
	}
	else
	{
		// if nothing got loaded, consider this tile an empty tile
		m_TileLoadState = ETileLoadState::Empty;
	}

	return m_TileLoadState == ETileLoadState::Loading;
}

void UGenericTwinOGCFeaturesTile::Unload()
{
}

void UGenericTwinOGCFeaturesTile::SetIsHidden(bool IsHidden)
{
	if(m_Visualiser)
	{
		m_Visualiser->SetActorHiddenInGame(IsHidden);
	}
}

void UGenericTwinOGCFeaturesTile::OnTileVisibilityChanged()
{
	if(m_Visualiser)
	{
		m_Visualiser->SetVisibility( m_TileVisibility > 0.0 );
	}
}

void UGenericTwinOGCFeaturesTile::OnResponse(int32 RequestId, const FString &Response)
{
	UE_LOG(LogGenericTwinOGCFeaturesTile, Log, TEXT("Response for request %d received: Len %d"), RequestId,  Response.Len());
	// UE_LOG(LogGenericTwinOGCFeaturesTile, Log, TEXT("%s"), *Response);

	m_Visualiser = m_Layer->CreateVisualiser();

	if(m_Visualiser)
	{
		m_Visualiser->SetActorLocation(m_WorldLocation);

		{
			class ProcessResultJob	:	public GenericTwin::JobBase
			{
			public:

				ProcessResultJob(UGenericTwinOGCFeaturesTile &Tile, const FString &JsonString, GenericTwin::OGCFeaturesCollection &FeaturesCollection)
					:	JobBase(GenericTwin::EJobType::Background)
					,	m_Tile(Tile)
					,	m_JsonString(JsonString)
					,	m_FeaturesCollection(FeaturesCollection)
				{
				}

				virtual GenericTwin::TJobBasePtr Execute() override
				{
					m_FeaturesCollection.LoadFromJson(m_JsonString);
					return GenericTwin::TJobBasePtr();
				}

				virtual void OnJobFinished()
				{
					m_Tile.OnFeaturesCollectionParsed();
				}

			private:

				UGenericTwinOGCFeaturesTile				&m_Tile;
				FString									m_JsonString;
				GenericTwin::OGCFeaturesCollection		&m_FeaturesCollection;
			};

			FGenericTwinJobDispatcher::DispatchJob(GenericTwin::TJobBasePtr( new ProcessResultJob(*this, Response, m_FeaturesCollection) ));

		}

	}

	m_TileLoadState = ETileLoadState::Loaded;
}

void UGenericTwinOGCFeaturesTile::OnRequestError(int32 RequestId, int32 ResponseCode, const FString &ErrorMessage)
{
	UE_LOG(LogGenericTwinOGCFeaturesTile, Log, TEXT("Response for request %d failed: ResponseCode %d"), RequestId,  ResponseCode);
	m_TileLoadState = ETileLoadState::Empty;
}


void UGenericTwinOGCFeaturesTile::OnFeaturesCollectionParsed()
{
	UE_LOG(LogGenericTwinOGCFeaturesTile, Log, TEXT("FeaturesCollection parsed") );
	m_Visualiser->SetFeaturesCollection(m_FeaturesCollection, m_Layer->GetGeoReferencingSystem(), m_WorldLocation);
	OnTileVisibilityChanged();
}

const TArray<TPair<FString, FString>> UGenericTwinOGCFeaturesTile::GetParameters(const FVector &TileLocation, bool AsCRS84)
{
	const FString crs = AsCRS84 ? TEXT("http://www.opengis.net/def/crs/OGC/1.3/CRS84") : TEXT("http://www.opengis.net/def/crs/EPSG/0/25832");

	TArray<TPair<FString, FString>> params	=	{	{TEXT("limit"), TEXT("100000")}
												,	{TEXT("bbox-crs"), crs}
												,	{TEXT("crs"), crs}
												};

	FString bbox;
	const float tileSizeM = m_Layer->GetTileSizeM();
	if(AsCRS84)
	{
		FGeographicCoordinates ll;
		FGeographicCoordinates ur;
		m_Layer->GetGeoReferencingSystem().ProjectedToGeographic(TileLocation, ll);
		m_Layer->GetGeoReferencingSystem().ProjectedToGeographic(TileLocation + FVector(tileSizeM, -tileSizeM, 0.0), ur);
		bbox =	FString::SanitizeFloat(ll.Longitude, 8) + TEXT(",")
			+	FString::SanitizeFloat(ll.Latitude, 8) + TEXT(",")
			+	FString::SanitizeFloat(ur.Longitude, 8) + TEXT(",")
			+	FString::SanitizeFloat(ur.Latitude, 8);

	}
	else
	{
		bbox =	FString::FromInt(static_cast<int32>(TileLocation.X)) + TEXT(",")
			+	FString::FromInt(static_cast<int32>(TileLocation.Y)) + TEXT(",")
			+	FString::FromInt(static_cast<int32>(TileLocation.X + tileSizeM)) + TEXT(",")
			+	FString::FromInt(static_cast<int32>(TileLocation.Y + tileSizeM));
	}

	params.Add({TEXT("bbox"), bbox});
	return params;
}

FString UGenericTwinOGCFeaturesTile::BuildRequestUrl(const FVector &TileLocation)
{
	/*
	https://api.hamburg.de/datasets/v1/strassenbaumkataster/collections/strassenbaumkataster/items?bbox-crs=http%3A%2F%2Fwww.opengis.net%2Fdef%2Fcrs%2FEPSG%2F0%2F25832&crs=http%3A%2F%2Fwww.opengis.net%2Fdef%2Fcrs%2FEPSG%2F0%2F25832&filter-crs=http%3A%2F%2Fwww.opengis.net%2Fdef%2Fcrs%2FEPSG%2F0%2F25832&filter-lang=cql2-text&limit=10&offset=0&skipGeometry=fals
	*/

	FString url = m_Layer->GetBaseUrl();

	if (url.EndsWith(TEXT("/")) == false)
		url += TEXT("/");

	url += TEXT("items?limit=100000&bbox-crs=http%3A%2F%2Fwww.opengis.net%2Fdef%2Fcrs%2FEPSG%2F0%2F25832&crs=http%3A%2F%2Fwww.opengis.net%2Fdef%2Fcrs%2FEPSG%2F0%2F25832&f=json&bbox=");

	const float m_TileSizeM = 500.0f;

	url += FString::FromInt(static_cast<int32>(TileLocation.X)) + TEXT(",");
	url += FString::FromInt(static_cast<int32>(TileLocation.Y)) + TEXT(",");
	url += FString::FromInt(static_cast<int32>(TileLocation.X + m_TileSizeM)) + TEXT(",");
	url += FString::FromInt(static_cast<int32>(TileLocation.Y + m_TileSizeM));

	return url;
}
